import { Component, OnInit } from '@angular/core';
import {IconMaterialsService} from '../../lib/Base/src/app/services/utility/icon.materials.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  constructor(private iconMaterialsService: IconMaterialsService) { }

  ngOnInit(): void {
    this.iconMaterialsService.registerAll();
  }

}
