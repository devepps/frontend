import { BrowserModule } from '@angular/platform-browser';
import {ActivatedRouteSnapshot, RouterModule, RouterStateSnapshot} from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';

import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatInputModule } from '@angular/material/input';
import { MatSnackBarModule } from '@angular/material/snack-bar';

import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CookieService } from 'ngx-cookie-service';
import {
  MatDialogModule,
  MatExpansionModule,
  MatIconModule,
  MatOptionModule,
  MatProgressBarModule,
  MatSelectModule, MatSortModule
} from '@angular/material';
import {PWDataSetDialogComponent} from '../components/pwDataSetDialog/pw.data.set.dialog.component';
import {PWDataViewerComponent} from '../components/pwDataViewer/pw.data.viewer.component';
import {PWViewerComponent} from '../components/pwViewer/pw.viewer.component';
import {HttpErrorInterceptor} from '../../lib/Base/src/app/network/interceptor.network';
import {DateToStringPipe} from '../../lib/Base/src/app/pipes/pipe.date.to.string';
import {ByteCountToStringPipe} from '../../lib/Base/src/app/pipes/pipe.bytecount.to.string';
import {DeleteDialogComponent} from '../../lib/Base/src/app/components/deletePopup/dialog.delete';
import {NotFoundComponent} from '../../lib/Base/src/app/components/notFound/notFound.component';
import {LoginComponent} from '../../lib/Base/src/app/components/login/login.component';
import {BannerComponent} from '../../lib/Base/src/app/components/banner/banner.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    BannerComponent,
    DateToStringPipe,
    ByteCountToStringPipe,
    DeleteDialogComponent,
    NotFoundComponent,
    PWDataSetDialogComponent,
    PWDataViewerComponent,
    PWViewerComponent,
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpErrorInterceptor,
      multi: true,
    },
    CookieService,
  ],
  bootstrap: [AppComponent],
  imports: [
    MatButtonModule,
    MatIconModule,
    MatCardModule,
    MatToolbarModule,
    MatInputModule,
    MatSnackBarModule,
    MatDialogModule,
    MatSelectModule,
    MatOptionModule,
    MatProgressBarModule,
    MatSortModule,
    FormsModule,
    BrowserModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    RouterModule.forRoot([
      {path: '', component: PWViewerComponent},
      {path: '404', component: NotFoundComponent},
      {path: '**', component: NotFoundComponent},
    ]),
    HttpClientModule,
    MatIconModule,
    MatExpansionModule,
  ]
})
export class AppModule { }
