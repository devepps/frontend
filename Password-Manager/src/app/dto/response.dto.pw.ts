import {PWDataEntry} from '../models/model.pw.entry';
import {PWDataSet} from '../models/model.pw.data';

export interface PWDataSetsDTO {
  pwDataSets: PWDataSet[];
}

export interface PWDataSetDTO {
  pwDataSet: PWDataSet[];
}

export interface PWDataEntryDTO {
  pwDataEntry: PWDataEntry[];
}

export interface PWDataEntryDTO {
  pwDataEntry: PWDataEntry[];
}
