import {Pipe, PipeTransform} from '@angular/core';
import {PWDataSet} from '../models/model.pw.data';
import {PWDataSetJDTO} from '../models/jdto/model.jdto.pw.data';
import {PWDataEntry} from '../models/model.pw.entry';
import {PWDataEntryJDTO} from '../models/jdto/model.jdto.pw.entry';

@Pipe({name: 'pwDataSetToJDTO'})
export class PWDataSetToJDTOPipe implements PipeTransform {
  private pwDataEntryToJDTOPipe = new PWDataEntryToJDTOPipe();

  transform(model: PWDataSet ): PWDataSetJDTO {
    if (model) {
      const entries = [];
      if (model.entries) {
        for (const entry of model.entries) {
          entries.push(this.pwDataEntryToJDTOPipe.transform(entry));
        }
      }
      return {
        entries: entries,
        id: model.id,
        name: model.name,
        created: ( model.created !== null && model.created !== undefined ) ?
          [model.created[0], model.created[1], model.created[2], model.created[3], model.created[4], model.created[5]]
          : null,
      };
    } else {
      return {
        entries: [],
        id: 0,
        name: '',
        created: null,
      };
    }
  }
}

@Pipe({name: 'pwDataEntryToJDTO'})
export class PWDataEntryToJDTOPipe implements PipeTransform {
  transform(model: PWDataEntry ): PWDataEntryJDTO {
    if (model) {
      return {
        content: model.content,
        id: model.id,
        key: model.key,
        created: ( model.created !== null && model.created !== undefined ) ?
          [model.created[0], model.created[1], model.created[2], model.created[3], model.created[4], model.created[5]]
          : null,
        isSecure: model.isSecure,
      };
    } else {
      return {
        content: '',
        id: 0,
        key: '',
        created: null,
        isSecure: false,
      };
    }
  }
}
