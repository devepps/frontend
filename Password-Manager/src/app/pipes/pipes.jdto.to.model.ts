import {Pipe, PipeTransform} from '@angular/core';
import {PWDataSetJDTO} from '../models/jdto/model.jdto.pw.data';
import {PWDataSet} from '../models/model.pw.data';
import {PWDataEntryJDTO} from '../models/jdto/model.jdto.pw.entry';
import {PWDataEntry} from '../models/model.pw.entry';

@Pipe({name: 'jdtoToPWDataSet'})
export class JdtoToPWDataSetPipe implements PipeTransform {
  private pwDataEntryToJDTOPipe = new JdtoToPWDataEntryPipe();

  transform(model: PWDataSetJDTO ): PWDataSet {
    if (model) {
      const entries = [];
      if (model.entries) {
        for (const entry of model.entries) {
          entries.push(this.pwDataEntryToJDTOPipe.transform(entry));
        }
      }
      return new PWDataSet(model.id, model.name, entries,
        (model.created === null) ? null : new Date(model.created[0], model.created[1] - 1, model.created[2], model.created[3], model.created[4], model.created[5]));
    } else {
      return new PWDataSet(0, '', [], null);
    }
  }
}

@Pipe({name: 'jdtoToPWDataEntry'})
export class JdtoToPWDataEntryPipe implements PipeTransform {
  transform(model: PWDataEntryJDTO ): PWDataEntry {
    if (model) {
      return new PWDataEntry(model.id, model.key, model.content,
        (model.created === null) ? null : new Date(model.created[0], model.created[1] - 1, model.created[2], model.created[3], model.created[4], model.created[5]),
        model.isSecure);
    } else {
      return new PWDataEntry(0, '', '', null, false);
    }
  }
}
