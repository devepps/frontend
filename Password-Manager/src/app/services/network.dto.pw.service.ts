import { Injectable } from '@angular/core';
import {PWDataSet} from '../models/model.pw.data';
import {PWDataEntry} from '../models/model.pw.entry';
import {DTOService} from '../../lib/Base/src/app/services/network.dto.service';

@Injectable({
  providedIn: 'root'
})
export class PWDTOService extends DTOService {

  public isPWDataSetIdsResponseMessage(validation: any): validation is number[] {
    return (<number[]>validation) !== undefined;
  }

  public isPWDataSetResponseMessage(validation: any): validation is PWDataSet[] {
    const value = (<PWDataSet[]>validation);
    return value !== undefined && value[0] !== undefined;
  }

  public isPWDataEntryResponseMessage(validation: any): validation is PWDataEntry[] {
    const value =  (<PWDataEntry[]>validation);
    return value !== undefined && value[0] !== undefined;
  }

}
