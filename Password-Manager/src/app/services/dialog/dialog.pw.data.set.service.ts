import {Injectable} from '@angular/core';
import {MatDialog} from '@angular/material';
import {PWDataSet} from '../../models/model.pw.data';
import {PWDataSetDialogComponent} from '../../components/pwDataSetDialog/pw.data.set.dialog.component';

@Injectable({
  providedIn: 'root'
})
export class PwDataSetDialogService {

  constructor(private dialog: MatDialog) {}

  public openCreateDialog(): void {
    let ref = this.dialog.open(PWDataSetDialogComponent, { data: null});
    ref.afterClosed().subscribe(() => {
      if (ref) {
        ref = null;
      }
    });
    ref.backdropClick().subscribe(() => {
      if (ref) {
        ref.close();
      }
    });
  }

  public openEditDialog(pwDataSet: PWDataSet): void {
    let ref = this.dialog.open(PWDataSetDialogComponent, { data: pwDataSet});
    ref.afterClosed().subscribe(() => {
      if (ref) {
        ref = null;
      }
    });
    ref.backdropClick().subscribe(() => {
      if (ref) {
        ref.close();
      }
    });
  }
}
