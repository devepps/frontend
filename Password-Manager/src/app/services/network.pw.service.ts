import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Observable} from 'rxjs';
import {PWDataSet} from '../models/model.pw.data';
import {PWDataEntryToJDTOPipe, PWDataSetToJDTOPipe} from '../pipes/pipes.model.to.jdto';
import {PWDataEntry} from '../models/model.pw.entry';
import {NetworkService} from '../../lib/Base/src/app/services/network.service';
import {environment} from '../../environments/environment';
import {ValidatedUser} from '../../lib/Base/src/app/models/model.validatedUser';

@Injectable({
  providedIn: 'root'
})
export class PWNetworkService extends NetworkService {

  protected baseHTTP_URL_PW: String = environment.baseHTTP_URL_PW;

  protected pwDataEntryToJDTOPipe = new PWDataEntryToJDTOPipe();
  protected pwDataSetToJDTOPipe = new PWDataSetToJDTOPipe();

  constructor(
    protected http: HttpClient) {
    super(http);
  }

  getPWDataSets(currentUser: ValidatedUser): Observable<any> {
    return this.http.get(this.baseHTTP_URL_PW + 'pwData/Sets/Ids',
      this.getValidatedHeader(currentUser));
  }

  getPWDataSetDatas(currentUser: ValidatedUser): Observable<any> {
    return this.http.get(this.baseHTTP_URL_PW + 'pwData/Sets/Data',
      this.getValidatedHeader(currentUser));
  }

  getPWDataSet(currentUser: ValidatedUser, pwDataSetId: number): Observable<any> {
    return this.http.get(this.baseHTTP_URL_PW + 'pwData/Set?id=' + pwDataSetId,
      this.getValidatedHeader(currentUser));
  }

  addPWDataSet(currentUser: ValidatedUser, pwDataSet: PWDataSet): Observable<any> {
    return this.http.put(this.baseHTTP_URL_PW + 'pwData/Set',
      '{\"pwDataSet\": ' + JSON.stringify(this.pwDataSetToJDTOPipe.transform(pwDataSet)) + '}',
      this.getValidatedHeader(currentUser));
  }

  updatePWDataSet(currentUser: ValidatedUser, pwDataSet: PWDataSet, id: number): Observable<any> {
    return this.http.post(this.baseHTTP_URL_PW + 'pwData/Set?id=' + id,
      '{\"pwDataSet\": ' + JSON.stringify(this.pwDataSetToJDTOPipe.transform(pwDataSet)) + '}',
      this.getValidatedHeader(currentUser));
  }

  deletePWDataSet(currentUser: ValidatedUser, id: number): Observable<any> {
    return this.http.delete(this.baseHTTP_URL_PW + 'pwData/Set?id=' + id,
      this.getValidatedHeader(currentUser));
  }

  addPWDataEntry(currentUser: ValidatedUser, pwDataEntry: PWDataEntry, setId: number): Observable<any> {
    return this.http.put(this.baseHTTP_URL_PW + 'pwData/Entry?setId=' + setId,
      '{\"pwDataEntry\": ' + JSON.stringify(this.pwDataEntryToJDTOPipe.transform(pwDataEntry)) + '}',
      this.getValidatedHeader(currentUser));
  }

  updatePWDataEntry(currentUser: ValidatedUser, pwDataEntry: PWDataEntry, setId: number): Observable<any> {
    return this.http.post(this.baseHTTP_URL_PW + 'pwData/Entry?setId=' + setId,
      '{\"pwDataEntry\": ' + JSON.stringify(this.pwDataEntryToJDTOPipe.transform(pwDataEntry)) + '}',
      this.getValidatedHeader(currentUser));
  }

  deletePWDataEntry(currentUser: ValidatedUser, entryId: number, setId: number): Observable<any> {
    return this.http.delete(this.baseHTTP_URL_PW + 'pwData/Entry?entryId=' + entryId + '&setId=' + setId,
      this.getValidatedHeader(currentUser));
  }
}
