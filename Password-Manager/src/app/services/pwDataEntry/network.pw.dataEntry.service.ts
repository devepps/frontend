import { Injectable } from '@angular/core';
import {PWDataEntry} from '../../models/model.pw.entry';
import {PWDataSet} from '../../models/model.pw.data';
import {PWNetworkService} from '../network.pw.service';
import {DTOService} from '../../../lib/Base/src/app/services/network.dto.service';
import {SnackBarService} from '../../../lib/Base/src/app/services/utility/snackbar.service';
import {UserService} from '../../../lib/Base/src/app/services/user/user.service';

@Injectable({
  providedIn: 'root'
})
export class NetworkPwDataEntryService {

  constructor(
    private networkService: PWNetworkService,
    private dtoService: DTOService,
    private userService: UserService,
    private snackBarService: SnackBarService) {
  }

  public deletePWDataEntry(pwDataEntry: PWDataEntry, pwDataSet: PWDataSet, onSuccess?: () => void): void {
    this.networkService.deletePWDataEntry(this.userService.getLastUser(), pwDataEntry.id, pwDataSet.id).subscribe(value => {
      if (!this.dtoService.isExceptionResponseMessage(value)) {
        this.snackBarService.openSnackBar('Data-Entry deleted.');
        if (onSuccess) {
          onSuccess();
        }
      } else if (this.dtoService.isExceptionResponseMessage(value)) {
        this.snackBarService.openSnackBar('Failed to deleted password-dataset.', value[0]);
      }
    });
  }
}
