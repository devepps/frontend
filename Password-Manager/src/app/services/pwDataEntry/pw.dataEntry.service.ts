import { Injectable } from '@angular/core';
import {NetworkPwDataEntryService} from './network.pw.dataEntry.service';
import {PWDataSet} from '../../models/model.pw.data';
import {PWDataEntry} from '../../models/model.pw.entry';
import {PwDataSetService} from '../pwDataSet/pw.dataSet.service';

@Injectable({
  providedIn: 'root'
})
export class PWDataEntryService {

  constructor(
    private networkPwDataEntryService: NetworkPwDataEntryService,
    private pwDataSetService: PwDataSetService) {
  }

  public deletePWDataEntry(pwDataEntry: PWDataEntry, pwDataSet: PWDataSet, onSuccess?: () => void): void {
    this.networkPwDataEntryService.deletePWDataEntry(pwDataEntry, pwDataSet, () => {
      pwDataSet.entries = pwDataSet.entries.filter(obj => obj !== pwDataEntry);
      this.pwDataSetService.updatePWDataSet(pwDataSet);
      if (onSuccess) {
        onSuccess();
      }
    });
  }
}
