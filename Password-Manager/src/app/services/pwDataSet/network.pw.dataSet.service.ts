import { Injectable } from '@angular/core';
import {Observable, Subject, Subscription} from 'rxjs';
import {PWDataSet} from '../../models/model.pw.data';
import {PWNetworkService} from '../network.pw.service';
import {PWDTOService} from '../network.dto.pw.service';
import {SnackBarService} from '../../../lib/Base/src/app/services/utility/snackbar.service';
import {UserService} from '../../../lib/Base/src/app/services/user/user.service';

@Injectable({
  providedIn: 'root'
})
export class NetworkPwDataSetService {

  private pwDataSets: Subject<PWDataSet[]> = new Subject<PWDataSet[]>();
  private lastPWDataSets = [];

  constructor(
    private networkService: PWNetworkService,
    private dtoService: PWDTOService,
    private userService: UserService,
    private snackBarService: SnackBarService) {
    this.getPWDataSets().subscribe((pwDataSets: PWDataSet[]) => {
      this.lastPWDataSets = pwDataSets;
    });
  }

  public getPWDataSets(): Observable<PWDataSet[]> {
    return this.pwDataSets.asObservable();
  }

  public updatePWDataSets(onSuccess?: () => any): void {
    this.networkService.getPWDataSetDatas(this.userService.getLastUser()).subscribe(value => {
      if (this.dtoService.isPWDataSetResponseMessage(value)) {
        this.pwDataSets.next(value);
        if (onSuccess) {
          onSuccess();
        }
      } else if (Array.isArray(value) && value.length === 0) {
        this.pwDataSets.next([]);
        if (onSuccess) {
          onSuccess();
        }
      } else if (this.dtoService.isExceptionResponseMessage(value)) {
        this.snackBarService.openSnackBar('Failed to update password-datasets.', value[0]);
      }
    });
  }

  public addPWDataSet(pwDataSet: PWDataSet, onSuccess?: (pwDataSet: PWDataSet) => void): void {
    this.networkService.addPWDataSet(this.userService.getLastUser(), pwDataSet).subscribe(value => {
      if (this.dtoService.isPWDataSetResponseMessage(value)) {
        onSuccess(value[0]);
        this.lastPWDataSets.push(value[0]);
        this.pwDataSets.next(this.lastPWDataSets);
        this.snackBarService.openSnackBar('Password-Dataset successfully saved.');
      } else if (this.dtoService.isExceptionResponseMessage(value)) {
        this.snackBarService.openSnackBar('Failed to save password-dataset.', value[0]);
      }
    });
  }

  public updatePWDataSet(pwDataSet: PWDataSet, onSuccess: (pwDataSet: PWDataSet) => void): Subscription {
    return this.networkService.updatePWDataSet(this.userService.getLastUser(), pwDataSet, pwDataSet.id).subscribe(value => {
      if (this.dtoService.isPWDataSetResponseMessage(value)) {
        onSuccess(value[0]);
        this.lastPWDataSets[this.lastPWDataSets.indexOf(pwDataSet)] = value[0];
        this.pwDataSets.next(this.lastPWDataSets);
        this.snackBarService.openSnackBar('Password-Dataset successfully updated.');
      } else if (this.dtoService.isExceptionResponseMessage(value)) {
        this.snackBarService.openSnackBar('Failed to update password-dataset.', value[0]);
      }
    });
  }

  public deletePWDataSet(pwDataSet: PWDataSet, onSuccess?: () => void): void {
    this.networkService.deletePWDataSet(this.userService.getLastUser(), pwDataSet.id).subscribe(value => {
      if (!this.dtoService.isExceptionResponseMessage(value)) {
        onSuccess();
        this.pwDataSets.next(this.lastPWDataSets.filter((obj: PWDataSet) => obj.id !== pwDataSet.id));
        this.snackBarService.openSnackBar('Password-Dataset successfully deleted.');
      } else if (this.dtoService.isExceptionResponseMessage(value)) {
        this.snackBarService.openSnackBar('Failed to deleted password-dataset.', value[0]);
      }
    });
  }

  public refresh(pwDataSets: PWDataSet[]): void {
    this.pwDataSets.next(pwDataSets);
  }
}
