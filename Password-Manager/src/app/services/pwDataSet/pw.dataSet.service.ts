import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {NetworkPwDataSetService} from './network.pw.dataSet.service';
import {PWDataSet} from '../../models/model.pw.data';

@Injectable({
  providedIn: 'root'
})
export class PwDataSetService {

  private sortBy = 'Date';
  private sortMode = 1;

  private lastPDDataSets: PWDataSet[] = [];

  constructor(
    private networkPwDataSetService: NetworkPwDataSetService) {
    this.getPWDataSets().subscribe((pwDataSets: PWDataSet[]) => {
      this.lastPDDataSets = pwDataSets;
    });
  }

  public getPWDataSets(): Observable<PWDataSet[]> {
    return this.networkPwDataSetService.getPWDataSets();
  }

  public updatePWDataSets(): void {
    this.networkPwDataSetService.updatePWDataSets(() => this.sort(this.sortBy, this.sortMode));
  }

  public addPWDataSet(pwDataSet: PWDataSet, onSuccess?: (pwDataSet: PWDataSet) => void): void {
    this.networkPwDataSetService.addPWDataSet(pwDataSet, onSuccess);
  }

  public updatePWDataSet(pwDataSet: PWDataSet, onSuccess?: (pwDataSet: PWDataSet) => void): void {
    this.networkPwDataSetService.updatePWDataSet(pwDataSet, onSuccess);
  }

  public deletePWDataSet(pwDataSet: PWDataSet, onSuccess?: () => void): void {
    this.networkPwDataSetService.deletePWDataSet(pwDataSet, onSuccess);
  }

  public sort(sortBy: string, mode: number): void {
    this.sortBy = sortBy;
    this.sortMode = mode;
    this.networkPwDataSetService.refresh(this.lastPDDataSets.sort((a: PWDataSet, b: PWDataSet) => {
      if (this.sortBy === 'Name') {
        return this.sortMode * a.name.localeCompare(b.name);
      } else if (this.sortBy === 'Date') {
        if (a.created === null || a.created === undefined || b.created === null || b.created === undefined) {
          return 0;
        } else {
          return this.sortMode * (a.created > b.created ? -1 : (a.created < b.created ? 1 : 0));
        }
      } else {
        return 0;
      }
    } ));
  }

  public clear() {
    this.networkPwDataSetService.refresh([]);
  }
}
