import {Component, Input, OnInit} from '@angular/core';
import { PWDataSet } from '../../models/model.pw.data';
import {PWDataEntry} from '../../models/model.pw.entry';
import {PWDataEntryService} from '../../services/pwDataEntry/pw.dataEntry.service';
import {PwDataSetService} from '../../services/pwDataSet/pw.dataSet.service';
import {PwDataSetDialogService} from '../../services/dialog/dialog.pw.data.set.service';
import {SnackBarService} from '../../../lib/Base/src/app/services/utility/snackbar.service';
import {StandardDialogService} from '../../../lib/Base/src/app/services/dialog/dialog.standard.service';

@Component({
  selector: 'pw-data-viewer',
  templateUrl: './pw.data.viewer.component.html',
  styleUrls: ['./pw.data.viewer.component.css']
})
export class PWDataViewerComponent implements OnInit{

  @Input() private pwDataSet: PWDataSet;

  private pwDataEntries: PWDataEntry[] = [];
  private pwDataEntriesCopy: PWDataEntry[] = [];

  constructor(private pwDataSetService: PwDataSetService,
              private pwDataEntryService: PWDataEntryService,
              private pwDataSetDialogService: PwDataSetDialogService,
              private snackBarService: SnackBarService,
              private standardDialogService: StandardDialogService) { }

  ngOnInit(): void {
    for (const entry of this.pwDataSet.entries) {
      this.pwDataEntriesCopy.push(new PWDataEntry(entry.id, entry.key, entry.content, entry.created, entry.isSecure));
    }
  }

  private save(): void {
    this.pwDataSet.entries = this.pwDataSet.entries.concat(this.pwDataEntries);
    this.pwDataEntries = [];
    this.pwDataSetService.updatePWDataSet(this.pwDataSet, (pwDataSet: PWDataSet) => {
      this.pwDataSet = pwDataSet;
    });
  }

  private add(): void {
    this.pwDataEntries.push(new PWDataEntry(0, '', '', null, false));
  }

  private isEdited(): boolean {
    let i = 0;
    while (i < this.pwDataEntriesCopy.length) {
      if (this.pwDataEntriesCopy[i].key !== this.pwDataSet.entries[i].key) {
        return true;
      }
      if (this.pwDataEntriesCopy[i].content !== this.pwDataSet.entries[i].content) {
        return true;
      }
      if (this.pwDataEntriesCopy[i].isSecure !== this.pwDataSet.entries[i].isSecure) {
        return true;
      }
      i++;
    }
    return this.pwDataEntries.length > 0;
  }

  public deleteEntry(pwEntry: PWDataEntry): void {
    if (this.pwDataEntries.includes(pwEntry)) {
      this.pwDataEntries = this.pwDataEntries.filter(obj => obj !== pwEntry);
    } else {
      if (pwEntry.isSecure) {
        this.standardDialogService.openRawDeleteDialog(pwEntry.key, (onOk: () => void) => {
          onOk();
          this.pwDataEntryService.deletePWDataEntry(pwEntry, this.pwDataSet, () => {
            this.pwDataEntriesCopy = this.pwDataEntriesCopy.filter(obj => obj.id !== pwEntry.id);
          });
        });
      } else {
        this.pwDataEntryService.deletePWDataEntry(pwEntry, this.pwDataSet, () => {
          this.pwDataEntriesCopy = this.pwDataEntriesCopy.filter(obj => obj.id !== pwEntry.id);
        });
      }
    }
  }

  public edit(): void {
    this.pwDataSetDialogService.openEditDialog(this.pwDataSet);
  }

  public switchIsSecure(pwEntry: PWDataEntry): void {
    if (pwEntry.isSecure) {
      pwEntry.isSecure = false;
    } else {
      pwEntry.isSecure = true;
    }
  }

  public copyContentToClipboard(pwEntry: PWDataEntry) {
    document.addEventListener('copy', (e: ClipboardEvent) => {
      e.clipboardData.setData('text/plain', (pwEntry.content));
      e.preventDefault();
      document.removeEventListener('copy', null);
    });
    document.execCommand('copy');
    this.snackBarService.openSnackBar('Copied Content');
  }
}
