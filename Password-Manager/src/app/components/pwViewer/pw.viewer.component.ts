import { Component, OnInit } from '@angular/core';
import { PWDataSet } from '../../models/model.pw.data';
import {PwDataSetDialogService} from '../../services/dialog/dialog.pw.data.set.service';
import {PwDataSetService} from '../../services/pwDataSet/pw.dataSet.service';
import {UserService} from '../../../lib/Base/src/app/services/user/user.service';

@Component({
  selector: 'pw-viewer',
  templateUrl: './pw.viewer.component.html',
  styleUrls: ['./pw.viewer.component.css']
})
export class PWViewerComponent implements OnInit {

  private currentPWDataSets: PWDataSet[];
  private sortBy = 'Date';
  private mode = 1;

  constructor(private pwDataSetService: PwDataSetService,
              private pwDataSetDialogService: PwDataSetDialogService,
              private userService: UserService) {
  }

  ngOnInit(): void {
    if (this.userService.isLoggedin()) {
      this.pwDataSetService.updatePWDataSets();
    } else {
      this.userService.getCurrentUser().subscribe(() => {
        if (this.userService.isLoggedin()) {
          this.pwDataSetService.updatePWDataSets();
        }
      });
      this.userService.validate();
    }
    this.pwDataSetService.getPWDataSets().subscribe((pwDataSets: PWDataSet[]) => {
      if (pwDataSets !== null && pwDataSets !== undefined) {
        this.currentPWDataSets = pwDataSets;
      } else {
        this.currentPWDataSets = [];
      }
    });
  }

  private newPWDataSet(): void {
    this.pwDataSetDialogService.openCreateDialog();
  }

  public changeSortMode(): void {
    if (this.mode === 1) {
      this.mode = -1;
    } else {
      this.mode = 1;
    }
    this.sort();
  }

  private sort(): void {
    this.pwDataSetService.sort(this.sortBy, this.mode);
  }
}
