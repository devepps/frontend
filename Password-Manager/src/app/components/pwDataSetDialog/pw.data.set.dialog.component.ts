import {Component, Inject, OnInit} from '@angular/core';
import { PWDataSet } from '../../models/model.pw.data';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {DataSetType} from '../../models/enum/model.enum.data.set.type';
import {PwDataSetService} from '../../services/pwDataSet/pw.dataSet.service';

@Component({
  selector: 'pw-data-set-dialog',
  templateUrl: './pw.data.set.dialog.component.html',
  styleUrls: ['./pw.data.set.dialog.component.css']
})
export class PWDataSetDialogComponent implements OnInit {

  private readonly dataSetTypes = DataSetType.DataSetTypes;

  private isNew = true;
  private selectedType = DataSetType.EmptyType;

  constructor(@Inject(MAT_DIALOG_DATA) private pwDataSet: PWDataSet,
              private pwDataSetService: PwDataSetService,
              public dialogRef: MatDialogRef<PWDataSetDialogComponent>) {
  }

  ngOnInit(): void {
    this.isNew = this.pwDataSet === null;
    if (this.pwDataSet === null) {
      this.pwDataSet = new PWDataSet(0, '', [], null);
    }
  }

  private add(): void {
    this.pwDataSetService.addPWDataSet(this.pwDataSet, (pwDataSet: PWDataSet) => {
      this.selectedType.config(pwDataSet);
      this.dialogRef.close();
    });
  }

  private delete(): void {
    this.pwDataSetService.deletePWDataSet(this.pwDataSet, () => {
      this.dialogRef.close();
    });
  }

  private save(): void {
    this.pwDataSetService.updatePWDataSet(this.pwDataSet, () => {
      this.dialogRef.close();
    });
  }
}
