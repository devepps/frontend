import {PWDataSet} from '../model.pw.data';
import {PWDataEntry} from '../model.pw.entry';

export class DataSetType {

  private static readonly  Charset = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890ß!"§$%&/()=?{[]}\\+*~#\'-_.:,;<>|';

  public static readonly EmptyType = new DataSetType('Empty', (pwDataSet: PWDataSet) => {});
  public static readonly AccountType = new DataSetType('Account', (pwDataSet: PWDataSet) => {
    pwDataSet.entries.push(new PWDataEntry(0, 'Username', '', null, false));
    pwDataSet.entries.push(new PWDataEntry(0, 'E-Mail', '', null, false));
    let pw = '';
    while (pw.length < 20) {
      pw += DataSetType.Charset.charAt(Math.random() * DataSetType.Charset.length);
    }
    pwDataSet.entries.push(new PWDataEntry(0, 'Password', pw, null, true));
  });

  public static readonly DataSetTypes: DataSetType[] = [DataSetType.EmptyType, DataSetType.AccountType];

  private readonly _name: string;
  private readonly _config: (pwDataSet: PWDataSet) => void;

  private constructor(name: string, config: (pwDataSet: PWDataSet) => void) {
    this._name = name;
    this._config = config;
  }

  public get name(): string {
    return this._name;
  }

  public get config(): (pwDataSet: PWDataSet) => void {
    return this._config;
  }
}
