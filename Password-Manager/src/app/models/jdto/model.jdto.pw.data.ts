import {PWDataEntryJDTO} from './model.jdto.pw.entry';

export class PWDataSetJDTO {

  public id: number;
  public name: string;
  public entries: PWDataEntryJDTO[];
  public created: number[];

}
