export class PWDataEntryJDTO {

  public id: number;
  public key: string;
  public content: string;
  public created: number[];
  public isSecure: boolean;

}
