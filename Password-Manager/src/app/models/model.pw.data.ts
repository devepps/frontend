import { PWDataEntry } from './model.pw.entry';

export class PWDataSet {

  private _id: number;
  private _name: string;
  private _entries: PWDataEntry[];
  private _created: Date;

  constructor(id: number, name: string, entries: PWDataEntry[], created: Date) {
    this._id = id;
    this._name = name;
    this._entries = entries;
    this._created = created;
  }

  get id(): number {
    return this._id;
  }

  get name(): string {
    return this._name;
  }

  set name(value: string) {
    this._name = value;
  }

  get entries(): PWDataEntry[] {
    return this._entries;
  }

  set entries(value: PWDataEntry[]) {
    this._entries = value;
  }

  get created(): Date {
    return this._created;
  }
}
