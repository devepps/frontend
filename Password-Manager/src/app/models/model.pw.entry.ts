export class PWDataEntry {

  private _id: number;
  private _key: string;
  private _content: string;
  private _created: Date;
  private _isSecure: boolean;

  constructor(id: number, key: string, content: string, created: Date, isSecure: boolean) {
    this._id = id;
    this._key = key;
    this._content = content;
    this._isSecure = isSecure;
    this._created = created;
  }

  get id(): number {
    return this._id;
  }

  get key(): string {
    return this._key;
  }

  set key(value: string) {
    this._key = value;
  }

  get content(): string {
    return this._content;
  }

  set content(value: string) {
    this._content = value;
  }

  get created(): Date {
    return this._created;
  }

  get isSecure(): boolean {
    return this._isSecure;
  }

  set isSecure(value: boolean) {
    this._isSecure = value;
  }
}
