import {Pipe, PipeTransform} from '@angular/core';
import {DecimalPipe} from '@angular/common';

@Pipe({name: 'dateToStringPipe'})
export class DateToStringPipe implements PipeTransform {
  decimalPipe: DecimalPipe = new DecimalPipe('en-Us');

  transform(model): string {
    if (model) {
      if (model instanceof Date && typeof(model.getDate) === 'function') {
        model = model as Date;
        return this.decimalPipe.transform(model.getDate(), '2.0')  + '.'
          + this.decimalPipe.transform(model.getMonth(), '2.0')  + '.'
          + model.getFullYear() + ' '
          +  this.decimalPipe.transform(model.getHours(), '2.0')
          + ':' + this.decimalPipe.transform(model.getMinutes(), '2.0');
      } else {
        return this.decimalPipe.transform(model[2], '2.0')  + '.'
          + this.decimalPipe.transform(model[1], '2.0')  + '.'
          + model[0] + ' '
          +  this.decimalPipe.transform(model[3], '2.0')
          + ':' + this.decimalPipe.transform(model[4], '2.0');
      }
    } else {
      return '';
    }
  }
}
