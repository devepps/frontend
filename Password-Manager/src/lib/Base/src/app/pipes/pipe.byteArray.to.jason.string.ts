import {Pipe, PipeTransform} from '@angular/core';

@Pipe({name: 'byteArrayToJasonStringPipe'})
export class ByteArrayToJasonStringPipe implements PipeTransform {

  private decoder = new TextDecoder('iso-8859-1');

  transform(model: ArrayBuffer): string {
    const data = Uint16Array.from(new Uint8Array(model), (v, k) => this.map(v));
    return this.decoder.decode(data);
  }
  public map(v: number): number {
    if (v === 34) { // "
      return 65;
    } else if (v === 39) { // '
       return 66;
    } else if (v === 38) { // &
       return 67;
     }  else if (v === 47) { // /
       return 68;
    } else if (v === 58) { // :
       return 69;
    } else if (v === 92) { // \
       return 70;
    } else if (v === 60) { // >
       return 71;
    } else if (v === 62) { // <
       return 72;
    } else if (v >= 127 && v <= 159) { // \n
       const newValue = 73 + v - 127;
       return newValue >= 90 ? newValue + 8 : newValue;
    } else {
       return v << 8;
    }
  }
}
