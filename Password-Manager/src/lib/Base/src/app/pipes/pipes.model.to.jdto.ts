import {Pipe, PipeTransform} from '@angular/core';
import {Login} from '../models/model.login';
import {User} from '../models/model.user';
import {ValidatedUser} from '../models/model.validatedUser';
import {Validation} from '../models/model.validation';
import {LoginJDTO} from '../models/jdto/model.jdto.login';
import {UserJDTO} from '../models/jdto/model.jdto.user';
import {ValidationJDTO} from '../models/jdto/model.jdto.validation';
import {ValidatedUserJDTO} from '../models/jdto/model.jdto.validatedUser';
@Pipe({name: 'loginToJDTO'})
export class LoginToJDTOPipe implements PipeTransform {
  transform(model: Login ): LoginJDTO {
    if (model) {
      return {
        username: model.username,
        password: model.password
      };
    } else {
      return {
        username: '',
        password: ''
      };
    }
  }
}

@Pipe({name: 'userToJDTO'})
export class UserToJDTOPipe implements PipeTransform {
  transform(model: User ): UserJDTO {
    if (model) {
      return {
        username: model.username,
        password: model.password,
        eMail: model.eMail
      };
    } else {
      return {
        username: '',
        password: '',
        eMail: ''
      };
    }
  }
}

@Pipe({name: 'validatedUserToJDTO'})
export class ValidatedUserToJDTOPipe implements PipeTransform {
  transform(model: ValidatedUser ): ValidatedUserJDTO {
    if (model) {
      return {
        userId: model.userId,
        validationKey: model.validationKey,
        username: model.username
      };
    } else {
      return {
        userId: '',
        username: '',
        validationKey: ''
      };
    }
  }
}

@Pipe({name: 'validationToJDTO'})
export class ValidationToJDTOPipe implements PipeTransform {
  transform(model: Validation ): ValidationJDTO {
    if (model) {
      return {
        userId: model.userId,
        validationKey: model.validationKey
      };
    } else {
      return {
        userId: '',
        validationKey: ''
      };
    }
  }
}
