import {Pipe, PipeTransform} from '@angular/core';

@Pipe({name: 'byteCountToStringPipe'})
export class ByteCountToStringPipe implements PipeTransform {
  transform(model: number): string {
    if (model) {
      if (model / 1024 < 1) {
        return model + ' B';
      } else if (model / 1024 < 1024) {
        return Math.ceil(model / 1024) + ' KB';
      } else if (model / (1024 * 1024) < 1024) {
        return Math.ceil(model / (1024 * 1024))  + ' MB';
      } else if (model / (1024 * 1024 * 1024) < 1024) {
        return Math.ceil(model / (1024 * 1024 * 1024))  + ' GB';
      } else {
        return model + 'B';
      }
    } else {
      return '0 B';
    }
  }
}
