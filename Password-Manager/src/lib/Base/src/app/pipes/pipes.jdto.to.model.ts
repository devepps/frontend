import {Pipe, PipeTransform} from '@angular/core';
import {User} from '../models/model.user';
import {ValidatedUser} from '../models/model.validatedUser';
import {Validation} from '../models/model.validation';
import {LoginJDTO} from '../models/jdto/model.jdto.login';
import {UserJDTO} from '../models/jdto/model.jdto.user';
import {ValidationJDTO} from '../models/jdto/model.jdto.validation';
import {ValidatedUserJDTO} from '../models/jdto/model.jdto.validatedUser';
import {Login} from '../models/model.login';

@Pipe({name: 'jdtoToLogin'})
export class JdtoToLoginPipe implements PipeTransform {
  transform(model: LoginJDTO ): Login {
    if (model) {
      return new Login(model.username, model.password);
    } else {
      return new Login('', '');
    }
  }
}

@Pipe({name: 'jdtoToUser'})
export class JdtoToUserPipe implements PipeTransform {
  transform(model: UserJDTO ): User {
    if (model) {
      return new User(model.username, model.password, model.eMail);
    } else {
      return new User('', '', '');
    }
  }
}

@Pipe({name: 'jdtoToValidatedUser'})
export class JdtoToValidatedUserPipe implements PipeTransform {
  transform(model: ValidatedUserJDTO ): ValidatedUser {
    if (model) {
      return new ValidatedUser(model.username, model.userId, model.validationKey);
    } else {
      return new ValidatedUser('', '', '');
    }
  }
}

@Pipe({name: 'jdtoToValidation'})
export class JdtoToValidationPipe implements PipeTransform {
  transform(model: ValidationJDTO ): Validation {
    if (model) {
      return new Validation(model.userId, model.validationKey);
    } else {
      return new Validation('', '');
    }
  }
}
