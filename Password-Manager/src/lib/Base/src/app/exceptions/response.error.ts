export class ExceptionResponse {

  private _message: string;

  get message(): string {
    return this._message;
  }
}
