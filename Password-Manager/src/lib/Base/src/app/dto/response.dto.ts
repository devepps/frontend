import { ExceptionResponse } from '../exceptions/response.error';
import { Validation } from '../models/model.validation';
import { ValidatedUser } from '../models/model.validatedUser';

export interface ExceptionResponseDTO {
  exceptionResponse: ExceptionResponse[];
}

export interface ValidationDTO {
  validation: Validation[];
}

export interface ValidatedUserDTO {
  validatedUser: ValidatedUser[];
}
