import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {DeleteDialogInfo} from '../../models/model.dialog.info.delete';

@Component({
  selector: 'popup-delete-dialog',
  templateUrl: './dialog.delete.html',
  styleUrls: ['./dialog.delete.css']
})
export class DeleteDialogComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) private deleteDialogInfo: DeleteDialogInfo,
              public dialogRef: MatDialogRef<DeleteDialogComponent>) {
  }

  ngOnInit(): void {
  }

  private delete(): void {
    this.deleteDialogInfo.deleteMethod(() => {
      this.dialogRef.close();
    });
  }
}
