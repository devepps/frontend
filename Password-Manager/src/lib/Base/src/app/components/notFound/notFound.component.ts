import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';


@Component({
  selector: 'notFound-window',
  templateUrl: './notFound.component.html',
  styleUrls: ['./notFound.component.css']
})
export class NotFoundComponent implements OnInit {

  ngOnInit(): void {}

  constructor(
    private router: Router)
  { }

  getRoute(): string {
    return this.router.url;
  }
}
