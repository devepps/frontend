import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';

import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatInputModule } from '@angular/material/input';
import { MatSnackBarModule } from '@angular/material/snack-bar';

import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppComponent } from './app.component';
import { LoginComponent } from '../components/login/login.component';
import { BannerComponent } from '../components/banner/banner.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpErrorInterceptor } from '../network/interceptor.network';
import { CookieService } from 'ngx-cookie-service';
import { RegisterComponent } from '../components/register/register.component';
import {
  MatDialogModule,
  MatExpansionModule,
  MatIconModule,
  MatOptionModule,
  MatProgressBarModule,
  MatSelectModule, MatSortModule
} from '@angular/material';
import {DateToStringPipe} from '../pipes/pipe.date.to.string';
import {ByteCountToStringPipe} from '../pipes/pipe.bytecount.to.string';
import {DeleteDialogComponent} from '../components/deletePopup/dialog.delete';
import {NotFoundComponent} from '../components/notFound/notFound.component';
import {HomeComponent} from '../components/home/home.component';
import {ExternalPageLoaderComponent} from '../components/externalPageLoader/external.pageLoader.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    BannerComponent,
    RegisterComponent,
    DateToStringPipe,
    ByteCountToStringPipe,
    DeleteDialogComponent,
    NotFoundComponent,
    HomeComponent,
    ExternalPageLoaderComponent,
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpErrorInterceptor,
      multi: true,
    },
    CookieService,
  ],
  bootstrap: [AppComponent],
  exports: [
    DateToStringPipe,
    ByteCountToStringPipe,
    DeleteDialogComponent,
    NotFoundComponent,
  ],
  imports: [
    MatButtonModule,
    MatIconModule,
    MatCardModule,
    MatToolbarModule,
    MatInputModule,
    MatSnackBarModule,
    MatDialogModule,
    MatSelectModule,
    MatOptionModule,
    MatProgressBarModule,
    MatSortModule,
    FormsModule,
    BrowserModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    RouterModule.forRoot([
      {path: '', component: HomeComponent},
      {path: 'register', component: RegisterComponent},
      {path: '404', component: NotFoundComponent},
      {path: '**', component: NotFoundComponent},
    ]),
    HttpClientModule,
    MatIconModule,
    MatExpansionModule,
  ]
})
export class AppBaseModule { }
