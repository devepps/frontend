import {Injectable} from '@angular/core';
import {MatDialog} from '@angular/material';
import {LoginComponent} from '../../components/login/login.component';
import {UserService} from '../user/user.service';
import {ValidatedUser} from '../../models/model.validatedUser';

@Injectable({
  providedIn: 'root'
})
export class LoginDialogService {

  constructor(private dialog: MatDialog,
              private userService: UserService) {}

  public openLoginDialog(): void {
    let ref = this.dialog.open(LoginComponent);
    let subscription = this.userService.getCurrentUser().subscribe((currentUser: ValidatedUser) => {
      if (currentUser) {
        if (ref) {
          ref.close();
        }
        if (subscription) {
          subscription.unsubscribe();
          subscription = null;
        }
      }
    });
    ref.afterClosed().subscribe(() => {
      if (ref) {
        ref = null;
      }
      if (subscription) {
        subscription.unsubscribe();
        subscription = null;
      }
    });
  }

}
