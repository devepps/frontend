export  class User {

  private _username: string;
  private _password: string;
  private _eMail: string;

  constructor(username: string, password: string, eMail: string) {
    this._username = username;
    this._password = password;
    this._eMail = eMail;
  }

  get username(): string {
    return this._username;
  }

  set username(value: string) {
    this._username = value;
  }

  get password(): string {
    return this._password;
  }

  set password(value: string) {
    this._password = value;
  }

  get eMail(): string {
    return this._eMail;
  }

  set eMail(value: string) {
    this._eMail = value;
  }
}
