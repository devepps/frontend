export class DeleteDialogInfo {

  private _name: string;
  private _deleteMethod: (value: () => void) => any;

  constructor(name: string, deleteMethod: (value: () => void) => any) {
    this._name = name;
    this._deleteMethod = deleteMethod;
  }

  get name(): string {
    return this._name;
  }

  get deleteMethod(): (value: () => void) => any {
    return this._deleteMethod;
  }
}
