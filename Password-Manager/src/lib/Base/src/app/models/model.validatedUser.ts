import { Validation } from './model.validation';

export class ValidatedUser {

  private _username: string;
  private _userId: string;
  private _validationKey: string;

  constructor(username: string, userId: string, validationKey : string) {
    this._username = username;
    this._userId = userId;
    this._validationKey = validationKey;
  }

  public setValidation(validation: Validation): void {
    if (validation != null) {
      this._userId = validation.userId;
      this._validationKey = validation.validationKey;
    }
  }

  get username(): string {
    return this._username;
  }

  set username(value: string) {
    this._username = value;
  }

  get userId(): string {
    return this._userId;
  }

  get validationKey(): string {
    return this._validationKey;
  }
}
