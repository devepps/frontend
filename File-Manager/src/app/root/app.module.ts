import { BrowserModule } from '@angular/platform-browser';
import {ActivatedRouteSnapshot, RouterModule, RouterStateSnapshot} from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';

import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatInputModule } from '@angular/material/input';
import { MatSnackBarModule } from '@angular/material/snack-bar';

import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CookieService } from 'ngx-cookie-service';
import {
  MatDialogModule,
  MatExpansionModule,
  MatIconModule, MatListModule, MatMenuModule,
  MatOptionModule, MatPaginatorModule,
  MatProgressBarModule,
  MatSelectModule, MatSortModule
} from '@angular/material';
import {HttpErrorInterceptor} from '../../lib/Base/src/app/network/interceptor.network';
import {DateToStringPipe} from '../../lib/Base/src/app/pipes/pipe.date.to.string';
import {ByteCountToStringPipe} from '../../lib/Base/src/app/pipes/pipe.bytecount.to.string';
import {DeleteDialogComponent} from '../../lib/Base/src/app/components/deletePopup/dialog.delete';
import {NotFoundComponent} from '../../lib/Base/src/app/components/notFound/notFound.component';
import {BaseComponent} from '../components/base/base.component';
import {FolderViewComponent} from '../components/folderViewComponent/folder.view.component';
import {PathViewerComponent} from '../components/pathViewComponent/path.viewer.component';
import {SectionListComponent} from '../components/sectionListComponent/section.list.component';
import {SectionNodeDialogComponent} from '../components/sectionNodeDialogComponent/section.node.dialog.component';
import {FolderNodeDialogComponent} from '../components/folderNodeDialogComponent/folder.node.dialog.component';
import {LoginComponent} from '../../lib/Base/src/app/components/login/login.component';
import {BannerComponent} from '../../lib/Base/src/app/components/banner/banner.component';
import {FileNodeDialogComponent} from '../components/fileNodeDialogComponent/file.node.dialog.component';

@NgModule({
  declarations: [
    AppComponent,
    BaseComponent,
    LoginComponent,
    BannerComponent,
    FolderViewComponent,
    PathViewerComponent,
    SectionListComponent,
    SectionNodeDialogComponent,
    FolderNodeDialogComponent,
    FileNodeDialogComponent,
    DateToStringPipe,
    ByteCountToStringPipe,
    DeleteDialogComponent,
    NotFoundComponent,
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpErrorInterceptor,
      multi: true,
    },
    CookieService,
  ],
  bootstrap: [AppComponent],
  imports: [
    MatButtonModule,
    MatIconModule,
    MatCardModule,
    MatToolbarModule,
    MatInputModule,
    MatSnackBarModule,
    MatDialogModule,
    MatSelectModule,
    MatOptionModule,
    MatProgressBarModule,
    MatSortModule,
    MatListModule,
    MatMenuModule,
    MatPaginatorModule,
    FormsModule,
    BrowserModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    RouterModule.forRoot([
      {path: '', component: BaseComponent},
      {path: '404', component: NotFoundComponent},
      {path: '**', component: NotFoundComponent},
    ]),
    HttpClientModule,
    MatIconModule,
    MatExpansionModule,
  ]
})
export class AppModule { }
