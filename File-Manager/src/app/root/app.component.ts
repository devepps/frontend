import { Component, OnInit } from '@angular/core';
import {IconMaterialsService} from '../../lib/Base/src/app/services/utility/icon.materials.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  constructor(private iconMaterialsService: IconMaterialsService) { }

  ngOnInit(): void {
    this.iconMaterialsService.registerAll();
    this.iconMaterialsService.register('arrow-right', 'assets/icon/arrow-right-icon.svg');
    this.iconMaterialsService.register('file', 'assets/icon/file-icon.svg');
    this.iconMaterialsService.register('folder', 'assets/icon/folder-icon.svg');
    this.iconMaterialsService.register('create-new-folder', 'assets/icon/create-new-folder-icon.svg');
    this.iconMaterialsService.register('more-horiz', 'assets/icon/more-horiz-icon.svg');
    this.iconMaterialsService.register('more-horiz', 'assets/icon/more-horiz-icon.svg');
    this.iconMaterialsService.register('more-horiz', 'assets/icon/more-horiz-icon.svg');
    this.iconMaterialsService.register('file-upload', 'assets/icon/file-upload-icon.svg');
    this.iconMaterialsService.register('folder-upload', 'assets/icon/folder-upload-icon.svg');
  }

}
