export class NodeType {

  public static readonly  File = new NodeType('File');
  public static readonly  Folder = new NodeType('Folder');
  public static readonly  Section = new NodeType('Section');

  private readonly _name: string;

  constructor(name: string) {
    this._name = name;
  }

  get name(): string {
    return this._name;
  }
}
