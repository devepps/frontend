export class FolderNodeJDTO {

  public id: number;
  public name: string;
  public created: number[];

  public fileNodeIds: number[];
  public subFolderIds: number[];

}
