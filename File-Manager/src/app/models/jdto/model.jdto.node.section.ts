export class SectionNodeJDTO {

  public id: number;
  public name: string;
  public created: number[];

  public fileNodeIds: number[];
  public subFolderIds: number[];

}
