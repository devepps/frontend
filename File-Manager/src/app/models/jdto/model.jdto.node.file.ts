export class FileNodeJDTO {

  public id: number;
  public name: string;
  public dataType: string;
  public size: number;
  public created: number[];

}
