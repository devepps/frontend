export class FileNode {

  private _id: number;
  private _name: string;
  private _dataType: string;
  private _size: number;
  private _created: Date;

  constructor(id: number, name: string, dataType: string, size: number, created: Date) {
    this._id = id;
    this._name = name;
    this._dataType = dataType;
    this._size = size;
    this._created = created;
  }

  get id(): number {
    return this._id;
  }

  get name(): string {
    return this._name;
  }

  get dataType(): string {
    return this._dataType;
  }

  get size(): number {
    return this._size;
  }

  get created(): Date {
    return this._created;
  }

  set name(value: string) {
    this._name = value;
  }

  set dataType(value: string) {
    this._dataType = value;
  }
}
