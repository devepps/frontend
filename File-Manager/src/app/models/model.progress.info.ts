import {Subject} from 'rxjs';

export class ProgressInfo {

  private _current: number;
  private _max: number;

  private readonly _subscriptions = [];

  constructor( max?: number, current?: number) {
    this._current = current ? current : 0.0;
    this._max = (max !== null && max !== undefined) ? max : 100;
  }

  get current(): number {
    return this._current;
  }

  set current(value: number) {
    this._current = value;
    if (this.current >= this._max) {
      for (const subscription of  this._subscriptions) {
        subscription();
      }
    }
  }

  get max(): number {
    return this._max;
  }

  set max(value: number) {
    this._max = value;
  }

  add(value?: number) {
    this.current = this.current + (value ? value : 1);
  }

  subscribeToFinish(subscription: () => void) {
    this._subscriptions.push(subscription);
  }

  isFinished() {
    return this._current >= this.max;
  }
}
