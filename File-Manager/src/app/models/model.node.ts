import {NodeType} from './enum/model.enum.node.type';
import {FileNode} from './model.node.file';
import {SectionNode} from './model.node.section';
import {FolderNode} from './model.node.folder';

export class NodeF {

  private readonly fileNode: FileNode = null;
  private readonly folderNode: FolderNode = null;
  private readonly sectionNode: SectionNode = null;

  private readonly _nodeType: NodeType;
  private _uploadStatus = 1.0;

  constructor(private _node: FileNode | FolderNode | SectionNode,
              nodeType?: NodeType) {
    if (_node) {
      if (_node instanceof  FileNode) {
        this.fileNode = _node;
        this._nodeType = NodeType.File;
      } else if (_node instanceof  FolderNode) {
        this.folderNode = _node;
        this._nodeType = NodeType.Folder;
      } else if (_node instanceof  SectionNode) {
        this.sectionNode = _node;
        this._nodeType = NodeType.Section;
      } else {
        this._nodeType = nodeType;
      }
    } else {
      this._nodeType = nodeType;
    }
  }

  get uploadStatus(): number {
    return this._uploadStatus;
  }

  set uploadStatus(value: number) {
    this._uploadStatus = value;
  }

  get nodeType(): NodeType {
    return this._nodeType;
  }

  public getFileNode(): FileNode {
    return this.fileNode;
  }

  public getFolderNode(): FolderNode {
    return this.folderNode;
  }

  public getSectionNode(): SectionNode {
    return this.sectionNode;
  }

  public isFile(): boolean {
    return this._nodeType === NodeType.File;
  }

  public isFolder(): boolean {
    return this._nodeType === NodeType.Folder;
  }

  public isSection(): boolean {
    return this._nodeType === NodeType.Section;
  }

  public isOk(): boolean {
    return this.nodeType !== null && (this.isFile() || this.isSection() || this.isFolder());
  }

  public getName() {
    if (this.isFile()) {
      return this.fileNode ? this.fileNode.name : null;
    } else if (this.isFolder()) {
      return this.folderNode ? this.folderNode.name : null;
    } else if (this.isSection()) {
      return this.sectionNode ? this.sectionNode.name : null;
    }
  }

  public getCreated() {
    if (this.isFile()) {
      return this.fileNode ? this.fileNode.created : null;
    } else if (this.isFolder()) {
      return this.folderNode ? this.folderNode.created : null;
    } else if (this.isSection()) {
      return this.sectionNode ? this.sectionNode.created : null;
    }
  }

  public getId() {
    if (this.isFile()) {
      return this.fileNode ? this.fileNode.id : null;
    } else if (this.isFolder()) {
      return this.folderNode ? this.folderNode.id : null;
    } else if (this.isSection()) {
      return this.sectionNode ? this.sectionNode.id : null;
    }
  }

  getDataType() {
    if (this.isFile()) {
      return this.fileNode ? this.fileNode.dataType : null;
    } else if (this.isFolder()) {
      return 'Folder';
    } else if (this.isSection()) {
      return 'Section';
    }
  }

  getSize() {
    if (this.isFile()) {
      return this.fileNode ? this.fileNode.size : null;
    } else if (this.isFolder()) {
      return 0;
    } else if (this.isSection()) {
      return 0;
    }
  }

  getFileNodeIds() {
    if (this.isFile()) {
      return null;
    } else if (this.isFolder()) {
      return this.folderNode ? this.folderNode.fileNodeIds : null;
    } else if (this.isSection()) {
      return this.sectionNode ? this.sectionNode.fileNodeIds : null;
    }
  }
}
