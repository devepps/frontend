export class Tree <KeyType, ContentType> {

  private _content: ContentType[] = [];
  private readonly _key: KeyType;
  private readonly _subNodes: Tree<KeyType, ContentType>[] = [];

  constructor(key: KeyType, content?: ContentType[]) {
    this._key = key;
    this._content = content ? content : [];
  }

  public updateContent(keys: KeyType[], content?: ContentType[],
                       onCreated?: (headTree: Tree<KeyType, ContentType>, created: Tree<KeyType, ContentType>) => void): ContentType[] {
    if (keys.length === 0) {
      this.content = content ? content : this.content;
      return this.content;
    }
    for (const subNode of this._subNodes) {
      if (subNode.key === keys[0]) {
        return subNode.updateContent(keys.slice(1), content, onCreated);
      }
    }
    const created = new Tree(keys[0], keys.length === 1 ? content : []);
    if (onCreated) {
      onCreated(this, created);
    }
    this._subNodes.push(created);
    if (keys.length === 1) {
      return created.content;
    }
    return created.updateContent(keys.slice(1), content, onCreated);
  }

  get content(): ContentType[] {
    return this._content;
  }

  set content(value: ContentType[]) {
    this._content = value;
  }

  get key(): KeyType {
    return this._key;
  }

  get subNodes(): Tree<KeyType, ContentType>[] {
    return this._subNodes;
  }
}
