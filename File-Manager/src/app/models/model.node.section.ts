export class SectionNode {

  private _id: number;
  private _name: string;
  private _created: Date;

  private _fileNodeIds: number[];
  private _subFolderIds: number[];

  constructor(id: number, name: string, created: Date, fileNodeIds: number[], subFolderIds: number[]) {
    this._id = id;
    this._name = name;
    this._created = created;
    this._fileNodeIds = fileNodeIds;
    this._subFolderIds = subFolderIds;
  }

  get id(): number {
    return this._id;
  }

  get name(): string {
    return this._name;
  }

  get created(): Date {
    return this._created;
  }

  get fileNodeIds(): number[] {
    return this._fileNodeIds;
  }

  get subFolderIds(): number[] {
    return this._subFolderIds;
  }

  set name(value: string) {
    this._name = value;
  }

  set fileNodeIds(value: number[]) {
    this._fileNodeIds = value;
  }

  set subFolderIds(value: number[]) {
    this._subFolderIds = value;
  }
}
