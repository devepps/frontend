import {SectionNode} from './model.node.section';
import {FolderNode} from './model.node.folder';
import {NodeF} from './model.node';

export class Path {

  private _section: SectionNode;
  private _folders: FolderNode[] = [];

  constructor(section: SectionNode) {
    this._section = section;
  }

  contains(folderNode: FolderNode) {
    return this._folders.filter((fn: FolderNode) => fn.id === folderNode.id).length === 1;
  }

  backTo(folderNode: FolderNode) {
    this._folders = this._folders.slice(0, this._folders.findIndex((fn: FolderNode) => fn.id === folderNode.id) + 1);
  }

  hasSubFolder(folderNode: FolderNode) {
    if (this._folders.length === 0) {
      return this.section.subFolderIds.filter((id: number) => folderNode.id === id).length !== 0;
    }
    return this._folders[this._folders.length - 1].subFolderIds.filter((fonId: number) => fonId === folderNode.id).length === 1;
  }

  top(): NodeF {
    if (this.folders.length === 0) {
      return new NodeF(this.section);
    } else {
      return new NodeF(this.folders[this.folders.length - 1]);
    }
  }

  next(folderNode: FolderNode) {
    this._folders.push(folderNode);
  }

  get section(): SectionNode {
    return this._section;
  }

  get folders(): FolderNode[] {
    return this._folders;
  }
}
