import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {FileNode} from '../../models/model.node.file';
import {NodeListService} from '../../services/nodeList/node.list.service';

@Component({
  selector: 'file-note-dialog',
  templateUrl: './file.node.dialog.component.html',
  styleUrls: ['./file.node.dialog.component.css']
})
export class FileNodeDialogComponent {

  private fileNodeCopy: FileNode;

  constructor(@Inject(MAT_DIALOG_DATA) private fileNode: FileNode,
              private nodeListService: NodeListService,
              public dialogRef: MatDialogRef<FileNodeDialogComponent>) {
    this.fileNodeCopy = new FileNode(fileNode.id, fileNode.name, fileNode.dataType, fileNode.size, fileNode.created);
  }

  private delete(): void {
    this.nodeListService.deleteFile(this.fileNodeCopy, () => {
      this.dialogRef.close();
    });
  }

  private save(): void {
    this.nodeListService.editFileNode(this.fileNode.id, this.fileNodeCopy, (answer) => {
      this.dialogRef.close();
    });
  }
}
