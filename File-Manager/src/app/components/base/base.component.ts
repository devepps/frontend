import { Component, OnInit } from '@angular/core';
import {UserService} from '../../../lib/Base/src/app/services/user/user.service';
import {SectionService} from '../../services/section/section.service';
import {ValidatedUser} from '../../../lib/Base/src/app/models/model.validatedUser';


@Component({
  selector: 'base-viewer',
  templateUrl: './base.component.html',
  styleUrls: ['./base.component.css']
})
export class BaseComponent implements OnInit {

  constructor(private userService: UserService,
              private sectionService: SectionService) {}

  ngOnInit(): void {
    this.userService.getCurrentUser().subscribe((user: ValidatedUser) => {
      this.sectionService.requestAllSections(user);
    });
  }
}
