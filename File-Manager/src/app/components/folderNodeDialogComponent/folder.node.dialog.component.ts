import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {FolderNode} from '../../models/model.node.folder';
import {NodeF} from '../../models/model.node';
import {NodeListService} from '../../services/nodeList/node.list.service';


@Component({
  selector: 'folder-node-dialog',
  templateUrl: './folder.node.dialog.component.html',
  styleUrls: ['./folder.node.dialog.component.css']
})
export class FolderNodeDialogComponent implements OnInit {

  private isNew = false;
  private folderNode: FolderNode;
  private headNode: NodeF;

  constructor(
    @Inject(MAT_DIALOG_DATA) private data,
    private nodeListService: NodeListService,
    public dialogRef: MatDialogRef<FolderNodeDialogComponent>)
  {}

  ngOnInit(): void {
    this.isNew = this.data.folderNode === null;
    this.folderNode = this.isNew ? new FolderNode(0, '', new Date(), [], []) : this.data.folderNode;
    this.headNode = this.data.headNode;
  }

  add() {
    this.nodeListService.addFolderNode(this.headNode, this.folderNode);
    this.dialogRef.close();
  }

  delete() {
    this.nodeListService.deleteFolderNode(this.folderNode);
    this.dialogRef.close();
  }

  save() {
    this.nodeListService.updateFolderNode(this.folderNode.id, this.folderNode);
    this.dialogRef.close();
  }
}
