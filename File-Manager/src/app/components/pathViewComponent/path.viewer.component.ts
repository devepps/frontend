import { Component, OnInit } from '@angular/core';
import {Path} from '../../models/model.path';
import {PathService} from '../../services/path/path.service';
import {SectionService} from '../../services/section/section.service';
import {FolderService} from '../../services/folder/folder.service';
import {FolderNode} from '../../models/model.node.folder';


@Component({
  selector: 'path-viewer',
  templateUrl: './path.viewer.component.html',
  styleUrls: ['./path.viewer.component.css']
})
export class PathViewerComponent implements OnInit {

  private path: Path;

  constructor(
    private pathService: PathService,
    private sectionService: SectionService,
    private folderService: FolderService,)
  {}

  ngOnInit(): void {
    this.pathService.getPath().subscribe((path: Path) => {
      this.path = path;
    });
  }

  selectSection() {
    this.sectionService.selectSection(this.path.section);
  }

  selectFolder(folderNode: FolderNode) {
    this.folderService.selectFolder(folderNode);
  }
}
