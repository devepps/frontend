import { Component, OnInit } from '@angular/core';
import {SectionNode} from '../../models/model.node.section';
import {SectionService} from '../../services/section/section.service';
import {SectionNodeDialogService} from '../../services/dialog/dialog.node.section.service';


@Component({
  selector: 'section-list-viewer',
  templateUrl: './section.list.component.html',
  styleUrls: ['./section.list.component.css']
})
export class SectionListComponent implements OnInit {

  private sectionNodes: SectionNode[];

  constructor(
    private sectionService: SectionService,
    private sectionNodeDialogService: SectionNodeDialogService)
  { }

  ngOnInit(): void {
    this.sectionService.getSectionNodes().subscribe((sectionNodes: SectionNode[]) => {
      this.sectionNodes = sectionNodes;
    });
  }

  selectSection(section: SectionNode) {
    this.sectionService.selectSection(section);
  }

  newSection() {
    this.sectionNodeDialogService.openCreateDialog();
  }
}
