import { Component, OnInit } from '@angular/core';
import {SectionNode} from '../../models/model.node.section';
import {SectionService} from '../../services/section/section.service';
import {SectionNodeDialogService} from '../../services/dialog/dialog.node.section.service';
import {NodeF} from '../../models/model.node';
import {NodeListService} from '../../services/nodeList/node.list.service';
import {ProgressInfo} from '../../models/model.progress.info';
import {PageEvent, Sort} from '@angular/material';
import {FileNode} from '../../models/model.node.file';
import {FileDataService} from '../../services/fileData/file.data.service';
import {PathService} from '../../services/path/path.service';
import {Path} from '../../models/model.path';
import {FolderService} from '../../services/folder/folder.service';
import {FolderNodeDialogService} from '../../services/dialog/dialog.node.folder.service';
import {Tree} from '../../models/model.tree';
import {FolderNode} from '../../models/model.node.folder';
import {Subject} from 'rxjs';
import {FileNodeDialogService} from '../../services/dialog/dialog.node.file.service';


@Component({
  selector: 'folder-viewer',
  templateUrl: './folder.view.component.html',
  styleUrls: ['./folder.view.component.css']
})
export class FolderViewComponent implements OnInit {

  private nodeList: NodeF[];
  private loadProgress: ProgressInfo;
  private selectedSection: SectionNode;
  private currentNode: NodeF;

  private pageIndex = 0;
  private pageSize = 10;

  constructor(
    private nodeListService: NodeListService,
    private sectionService: SectionService,
    private fileDataService: FileDataService,
    private path: PathService,
    private folderService: FolderService,
    private sectionNodeDialogService: SectionNodeDialogService,
    private folderNodeDialogService: FolderNodeDialogService,
    private fileNodeDialogService: FileNodeDialogService)
  {}

  ngOnInit(): void {
    this.nodeListService.getProgress().subscribe((loadProgress: ProgressInfo) => {
      this.loadProgress = loadProgress;
    });
    this.nodeListService.getList().subscribe((nodeList: NodeF[]) => {
      this.nodeList = nodeList;
    });
    this.sectionService.getSelectedSection().subscribe((section: SectionNode) => {
      this.selectedSection = section;
    });
    this.path.getPath().subscribe((path: Path) => {
      this.currentNode = path.top();
    });
  }

  sortData(sort: Sort) {
    this.nodeListService.sortList(sort);
  }

  download(node: NodeF) {
    if (node.isFile()) {
      this.fileDataService.downloadFileData(node.getFileNode());
    }
  }

  edit(node: NodeF) {
    if (node.isSection()) {
      this.sectionNodeDialogService.openEditDialog(node.getSectionNode());
    } else if (node.isFolder()) {
      this.folderNodeDialogService.openEditDialog(node.getFolderNode());
    } else if (node.isFile()) {
      this.fileNodeDialogService.openEditDialog(node.getFileNode());
    }
  }

  uploadFile(files: FileList) {
    let index = 0;
    while (index < files.length) {
      const file = files.item(index);
      const fileNode = new FileNode(0, file.name.substr(0, file.name.lastIndexOf('.')), file.name.substr(file.name.lastIndexOf('.') + 1, file.name.length), file.size, new Date());
      this.nodeListService.addUploading(fileNode, file);
      index++;
    }
  }

  uploadFolder(files: FileList) {
    const pathTree = new Tree<string, NodeF>('Base', [this.currentNode]);
    const newFolderObserver: Subject<Tree<string, NodeF>> = new Subject<Tree<string, NodeF>>();
    const newFolderObservable = newFolderObserver.asObservable();
    const folderObserver: Subject<any> = new Subject<any>();
    const folderObservable = folderObserver.asObservable();
    let index = 0;
    while (index < files.length) {
      const file = files.item(index);
      let path = (file as any).webkitRelativePath;
      path = path.substring(0, path.lastIndexOf('/')).split('/');
      const folder = pathTree.updateContent(path, [], (headTree, created) => {
        newFolderObservable.subscribe((info) => {
          if (info === headTree) {
            this.nodeListService.addFolderNode(headTree.content[0], new FolderNode(0, created.key, new Date(), [], []), folderNode => {
              created.content = [new NodeF(folderNode)];
              newFolderObserver.next(created);
              if (created.key === path[path.length - 1]) {
                folderObserver.next({headNode: created.content[0], filePath: ''.concat(path)});
              }
            });
          }
        });
        if (headTree.content.length === 1) {
          newFolderObserver.next(headTree);
        }
      });
      const observer = (headNode: NodeF) => {
        const fileNode = new FileNode(0,
          file.name.substr(0, file.name.lastIndexOf('.')), file.name.substr(file.name.lastIndexOf('.') + 1, file.name.length),
          file.size, new Date());
        this.nodeListService.addUploading(fileNode, file, headNode);
      };
      if (folder[0] === null || folder[0] === undefined) {
        folderObservable.subscribe((data) => {
          if (data.filePath === ''.concat(path)) {
            observer(data.headNode);
          }
        });
      } else {
        observer(folder[0]);
      }
      index++;
    }
  }

  downloadFolder() {

  }

  createFolder() {
    this.folderNodeDialogService.openCreateDialog(this.currentNode);
  }

  navigate(node: NodeF) {
    if (node.isFolder()) {
      this.folderService.selectFolder(node.getFolderNode());
    }
  }

  selectPage(pageEvent: PageEvent) {
    this.pageIndex = pageEvent.pageIndex;
    this.pageSize = pageEvent.pageSize;
  }
}
