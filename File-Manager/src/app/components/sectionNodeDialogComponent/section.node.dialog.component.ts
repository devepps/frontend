import {Component, Inject, OnInit} from '@angular/core';
import {SectionNode} from '../../models/model.node.section';
import {SectionService} from '../../services/section/section.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';


@Component({
  selector: 'section-node-dialog',
  templateUrl: './section.node.dialog.component.html',
  styleUrls: ['./section.node.dialog.component.css']
})
export class SectionNodeDialogComponent implements OnInit {

  private isNew = false;

  constructor(
    @Inject(MAT_DIALOG_DATA) private sectionNode: SectionNode,
    private sectionService: SectionService,
    public dialogRef: MatDialogRef<SectionNodeDialogComponent>)
  {}

  ngOnInit(): void {
    this.isNew = this.sectionNode === null;
    this.sectionNode = this.isNew ? new SectionNode(0, '', new Date(), [], []) : this.sectionNode;
  }

  add() {
    this.sectionService.addSectionNode(this.sectionNode);
    this.dialogRef.close();
  }

  delete() {
    this.sectionService.deleteSectionNode(this.sectionNode);
    this.dialogRef.close();
  }

  save() {
    this.sectionService.updateSectionNode(this.sectionNode.id, this.sectionNode);
    this.dialogRef.close();
  }
}
