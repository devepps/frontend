import {Injectable} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {Path} from '../../models/model.path';
import {SectionService} from '../section/section.service';
import {FolderService} from '../folder/folder.service';
import {SectionNode} from '../../models/model.node.section';
import {FolderNode} from '../../models/model.node.folder';

@Injectable({
  providedIn: 'root'
})
export class PathService {

  private path: Subject<Path> = new Subject<Path>();
  private lastPath: Path;

  constructor(private sectionService: SectionService,
              private folderService: FolderService) {
    this.sectionService.getSelectedSection().subscribe((sectionNode: SectionNode) => {
      this.path.next(new Path(sectionNode));
    });
    this.folderService.getSelectedFolder().subscribe((folderNode: FolderNode) => {
      if (this.lastPath.contains(folderNode)) {
        this.lastPath.backTo(folderNode);
      } else if (this.lastPath.hasSubFolder(folderNode)) {
        this.lastPath.next(folderNode);
      } else {
        // requestPath
      }
      this.path.next(this.lastPath);
    });
    this.getPath().subscribe((path: Path) => {
      this.lastPath = path;
    });
  }

  getPath(): Observable<Path> {
    return this.path.asObservable();
  }
}
