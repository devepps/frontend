import {Injectable} from '@angular/core';
import {NetworkFileDataService} from './network.file.data.service';
import {SnackBarService} from '../../../lib/Base/src/app/services/utility/snackbar.service';
import {FileNode} from '../../models/model.node.file';
import {FolderNode} from '../../models/model.node.folder';
import {SectionNode} from '../../models/model.node.section';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class FileDataService {

  private baseHTTP_URL_File = environment.baseHTTP_URL_File;

  constructor(
    private networkFileDataService: NetworkFileDataService,
    private snackBarService: SnackBarService) {
  }

  public uploadFileData(fileNode: FileNode, file: File, folderNode: FolderNode | SectionNode, onAnswer?: (value: FileNode) => any, onProgress?: (total, loaded) => any): void {
    this.networkFileDataService.uploadFile(fileNode, file, folderNode, (value: FileNode) => {
      if (onAnswer) {
        onAnswer(value);
      }
    }, (total, loaded) => {
      if (onProgress) {
        onProgress(total, loaded);
      }
    });
  }

  downloadFileData(fileNode: FileNode) {
    this.networkFileDataService.downloadFileData(fileNode, (securityId) => {
      const anchor = document.createElement('a');
      anchor.href = this.baseHTTP_URL_File + 'data/raw?id=' + fileNode.id + '&securityId=' + securityId;
      anchor.download = fileNode.name + '.' + fileNode.dataType;
      anchor.click();
      anchor.parentNode.removeChild(anchor);
    });
  }
}
