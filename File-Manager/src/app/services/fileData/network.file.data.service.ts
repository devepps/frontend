import {Injectable} from '@angular/core';
import {FileNetworkService} from '../network.file.service';
import {FileJDTOService} from '../network.file.jdto.service';
import {UserService} from '../../../lib/Base/src/app/services/user/user.service';
import {SnackBarService} from '../../../lib/Base/src/app/services/utility/snackbar.service';
import {JdtoToFileNodePipe} from '../../pipes/pipes.jdto.to.model';
import {FileNode} from '../../models/model.node.file';
import {FolderNode} from '../../models/model.node.folder';
import {SectionNode} from '../../models/model.node.section';

@Injectable({
  providedIn: 'root'
})
export class NetworkFileDataService {

  private jdtoToFileNodePipe = new JdtoToFileNodePipe();

  constructor(private fileNetworkService: FileNetworkService,
              private fileJDTOService: FileJDTOService,
              private userService: UserService,
              private snackBarService: SnackBarService) {
  }

  requestFileNode(fileId: number, onSuccess?: (FileNode) => void, onError?: (any) => void) {
    this.fileNetworkService.getFileNode(fileId, this.userService.getLastUser()).subscribe((value) => {
      if (this.fileJDTOService.isFileJDTOMessage(value)) {
        const fileNode = this.jdtoToFileNodePipe.transform(value[0]);
        if (onSuccess) { onSuccess(fileNode); }
      } else if (this.fileJDTOService.isExceptionResponseMessage(value)) {
        if (onError) { onError(value); }
        this.snackBarService.openSnackBar('Failed to request file', value[0]);
      } else {
        if (onError) { onError(null); }
        this.snackBarService.openSnackBar('An unknown error occurred while requesting file', value[0]);
      }
    });
  }

  uploadFile(fileNode: FileNode, file: File, folderNode: FolderNode | SectionNode, onAnswer: (answer: FileNode) => void, onProgress: (total, loaded) => void) {
    this.fileNetworkService.uploadFile(this.userService.getLastUser(), fileNode, file, folderNode, onProgress).subscribe((value) => {
      if (this.fileJDTOService.isFileJDTOMessage(value)) {
        if (onAnswer) {
          onAnswer(this.jdtoToFileNodePipe.transform(value[0]));
        }
      } else if (this.fileJDTOService.isExceptionResponseMessage(value)) {
        this.snackBarService.openSnackBar('Upload of file ' + fileNode.name + ' has failed.', value[0]);
        if (onAnswer) {
          onAnswer(null);
        }
      }
    });
  }

  downloadFileData(fileNode: FileNode, onSuccess: (securityId) => void) {
    this.fileNetworkService.startDownloadFileData(this.userService.getLastUser(), fileNode.id).subscribe((securityId: string) => {
      if (securityId && securityId.length > 0) {
        onSuccess(securityId);
      } else if (this.fileJDTOService.isExceptionResponseMessage(securityId)) {
        this.snackBarService.openSnackBar('Download of file ' + fileNode.name + ' has failed.', securityId[0]);
      }
    });
  }
}
