import {Injectable} from '@angular/core';
import {MatDialog} from '@angular/material';
import {FileNodeDialogComponent} from '../../components/fileNodeDialogComponent/file.node.dialog.component';
import {FileNode} from '../../models/model.node.file';

@Injectable({
  providedIn: 'root'
})
export class FileNodeDialogService {

  constructor(private dialog: MatDialog) {}

  public openEditDialog(fileNode: FileNode): void {
    let ref = this.dialog.open(FileNodeDialogComponent, { data: fileNode});
    ref.afterClosed().subscribe(() => {
      if (ref) {
        ref = null;
      }
    });
    ref.backdropClick().subscribe(() => {
      if (ref) {
        ref.close();
      }
    });
  }
}
