import {Injectable} from '@angular/core';
import {MatDialog} from '@angular/material';
import {SectionNode} from '../../models/model.node.section';
import { SectionNodeDialogComponent } from 'app/components/sectionNodeDialogComponent/section.node.dialog.component';

@Injectable({
  providedIn: 'root'
})
export class SectionNodeDialogService {

  constructor(private dialog: MatDialog) {}

  public openCreateDialog(): void {
    let ref = this.dialog.open(SectionNodeDialogComponent, { data: null});
    ref.afterClosed().subscribe(() => {
      if (ref) {
        ref = null;
      }
    });
    ref.backdropClick().subscribe(() => {
      if (ref) {
        ref.close();
      }
    });
  }

  public openEditDialog(sectionNode: SectionNode): void {
    let ref = this.dialog.open(SectionNodeDialogComponent, { data: sectionNode});
    ref.afterClosed().subscribe(() => {
      if (ref) {
        ref = null;
      }
    });
    ref.backdropClick().subscribe(() => {
      if (ref) {
        ref.close();
      }
    });
  }
}
