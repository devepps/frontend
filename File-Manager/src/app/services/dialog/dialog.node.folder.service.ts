import {Injectable} from '@angular/core';
import {MatDialog} from '@angular/material';
import { SectionNodeDialogComponent } from 'app/components/sectionNodeDialogComponent/section.node.dialog.component';
import {FolderNodeDialogComponent} from '../../components/folderNodeDialogComponent/folder.node.dialog.component';
import {NodeF} from '../../models/model.node';
import {FolderNode} from '../../models/model.node.folder';

@Injectable({
  providedIn: 'root'
})
export class FolderNodeDialogService {

  constructor(private dialog: MatDialog) {}

  public openCreateDialog(headNode: NodeF): void {
    let ref = this.dialog.open(FolderNodeDialogComponent, { data: {folderNode: null, headNode: headNode}});
    ref.afterClosed().subscribe(() => {
      if (ref) {
        ref = null;
      }
    });
    ref.backdropClick().subscribe(() => {
      if (ref) {
        ref.close();
      }
    });
  }

  public openEditDialog(folderNode: FolderNode): void {
    let ref = this.dialog.open(FolderNodeDialogComponent, { data: {folderNode: folderNode, headNode: null}});
    ref.afterClosed().subscribe(() => {
      if (ref) {
        ref = null;
      }
    });
    ref.backdropClick().subscribe(() => {
      if (ref) {
        ref.close();
      }
    });
  }
}
