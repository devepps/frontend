import {Injectable} from '@angular/core';
import {FileNetworkService} from '../network.file.service';
import {FileJDTOService} from '../network.file.jdto.service';
import {UserService} from '../../../lib/Base/src/app/services/user/user.service';
import {SnackBarService} from '../../../lib/Base/src/app/services/utility/snackbar.service';
import {JdtoToFileNodePipe} from '../../pipes/pipes.jdto.to.model';
import {FileNode} from '../../models/model.node.file';

@Injectable({
  providedIn: 'root'
})
export class NetworkFileNodeService {

  private jdtoToFileNodePipe = new JdtoToFileNodePipe();

  constructor(private fileNetworkService: FileNetworkService,
              private fileJDTOService: FileJDTOService,
              private userService: UserService,
              private snackBarService: SnackBarService) {
  }

  requestFileNode(fileId: number, onSuccess?: (FileNode) => void, onError?: (any) => void) {
    this.fileNetworkService.getFileNode(fileId, this.userService.getLastUser()).subscribe((value) => {
      if (this.fileJDTOService.isFileJDTOMessage(value)) {
        const fileNode = this.jdtoToFileNodePipe.transform(value[0]);
        if (onSuccess) { onSuccess(fileNode); }
      } else if (this.fileJDTOService.isExceptionResponseMessage(value)) {
        if (onError) { onError(value); }
        this.snackBarService.openSnackBar('Failed to request file', value[0]);
      } else {
        if (onError) { onError(null); }
        this.snackBarService.openSnackBar('An unknown error occurred while requesting file', value[0]);
      }
    });
  }

  deleteFileNode(fileNode: FileNode, onSuccess?: () => void, onError?: (any) => void) {
    this.fileNetworkService.deleteFileNode(fileNode.id, this.userService.getLastUser()).subscribe((value) => {
      if (this.fileJDTOService.isEmptyMessage(value)) {
        if (onSuccess) { onSuccess(); }
      } else if (this.fileJDTOService.isExceptionResponseMessage(value)) {
        if (onError) { onError(value); }
        this.snackBarService.openSnackBar('Failed to delete file', value[0]);
      } else {
        if (onError) { onError(null); }
        this.snackBarService.openSnackBar('An unknown error occurred while deleting file', value[0]);
      }
    });
  }

  updateFileNode(fileNodeId: number, fileNode: FileNode, onSuccess: (answerFile: FileNode) => void, onError: (any) => void) {
    this.fileNetworkService.updateFileNode(fileNodeId, fileNode, this.userService.getLastUser()).subscribe((value) => {
      if (this.fileJDTOService.isFileJDTOMessage(value)) {
        const fileNode = this.jdtoToFileNodePipe.transform(value[0]);
        if (onSuccess) { onSuccess(fileNode); }
      } else if (this.fileJDTOService.isExceptionResponseMessage(value)) {
        if (onError) { onError(value); }
        this.snackBarService.openSnackBar('Failed to update file', value[0]);
      } else {
        if (onError) { onError(null); }
        this.snackBarService.openSnackBar('An unknown error occurred while update file', value[0]);
      }
    });
  }
}
