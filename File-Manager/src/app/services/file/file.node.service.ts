import {Injectable} from '@angular/core';
import {NetworkFileNodeService} from './network.file.node.service';
import {FileNode} from '../../models/model.node.file';

@Injectable({
  providedIn: 'root'
})
export class FileNodeService {

  constructor(private networkFileNodeService: NetworkFileNodeService, ) {}

  requestFileNode(fileId: number, onSuccess?: (FileNode) => void, onError?: (any) => void) {
    this.networkFileNodeService.requestFileNode(fileId, onSuccess, onError);
  }

  deleteFileNode(fileNode: FileNode, onSuccess?: () => void, onError?: (any) => void) {
    this.networkFileNodeService.deleteFileNode(fileNode, onSuccess, onError);
  }

  updateFileNode(fileNodeId: number, fileNode: FileNode, onSuccess: (answerFile: FileNode) => void, onError?: (any) => void) {
    this.networkFileNodeService.updateFileNode(fileNodeId, fileNode, onSuccess, onError);
  }
}
