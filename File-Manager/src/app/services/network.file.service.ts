import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {NetworkService} from '../../lib/Base/src/app/services/network.service';
import {SectionNode} from '../models/model.node.section';
import {ValidatedUser} from '../../lib/Base/src/app/models/model.validatedUser';
import {environment} from '../../environments/environment';
import {FileNodeToJDTOPipe, FolderNodeToJDTOPipe, SectionNodeToJDTOPipe} from '../pipes/pipes.model.to.jdto';
import {NodeType} from '../models/enum/model.enum.node.type';
import {FolderNode} from '../models/model.node.folder';
import {NodeF} from '../models/model.node';
import {FileNode} from '../models/model.node.file';
import {Observable} from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class FileNetworkService extends NetworkService {

  private baseHTTP_URL_File = environment.baseHTTP_URL_File;

  constructor(
    protected http: HttpClient) {
    super(http);
  }

  private sectionNodeToJDTOPipe = new SectionNodeToJDTOPipe();
  private folderNodeToJDTOPipe = new FolderNodeToJDTOPipe();
  private fileNodeToJDTOPipe = new FileNodeToJDTOPipe();

  public addSectionNode(sectionNode: SectionNode, validatedUser: ValidatedUser) {
    return this.http.put(this.baseHTTP_URL_File + 'nodes/sections',
      '{\"section\": ' + JSON.stringify(this.sectionNodeToJDTOPipe.transform(sectionNode)) + '}',
      this.getValidatedHeader(validatedUser));
  }

  deleteSectionNode(sectionNode: SectionNode, validatedUser: ValidatedUser) {
    return this.http.delete(this.baseHTTP_URL_File + 'nodes/sections?id=' + sectionNode.id,
      this.getValidatedHeader(validatedUser));
  }

  updateSectionNode(sectionId: number, sectionNode: SectionNode, validatedUser: ValidatedUser) {
    return this.http.post(this.baseHTTP_URL_File + 'nodes/sections?id=' + sectionId,
      '{\"section\": ' + JSON.stringify(this.sectionNodeToJDTOPipe.transform(sectionNode)) + '}',
      this.getValidatedHeader(validatedUser));
  }

  addFolderNode(headNode: NodeF, folderNode: FolderNode, validatedUser: ValidatedUser) {
    if (headNode.isSection()) {
      const headSectionNode = headNode.getSectionNode();
      return this.http.put(this.baseHTTP_URL_File + 'nodes/folders/section',
        '{\"folder\": ' + JSON.stringify(this.folderNodeToJDTOPipe.transform(folderNode)) + ',' +
        '\"section\": ' + JSON.stringify(this.sectionNodeToJDTOPipe.transform(headSectionNode)) + '}',
        this.getValidatedHeader(validatedUser));
    } else {
      const headFolderNode = headNode.getFolderNode();
      return this.http.put(this.baseHTTP_URL_File + 'nodes/folders/folder',
        '{\"folder\": ' + JSON.stringify(this.folderNodeToJDTOPipe.transform(folderNode)) + ',' +
        '\"headFolder\": ' + JSON.stringify(this.folderNodeToJDTOPipe.transform(headFolderNode)) + '}',
        this.getValidatedHeader(validatedUser));
    }
  }

  deleteFolderNode(folderNode: FolderNode, validatedUser: ValidatedUser) {
    return this.http.delete(this.baseHTTP_URL_File + 'nodes/folders?id=' + folderNode.id,
      this.getValidatedHeader(validatedUser));
  }

  updateFolderNode(folderId: number, folderNode: FolderNode, validatedUser: ValidatedUser) {
    return this.http.post(this.baseHTTP_URL_File + 'nodes/folders?id=' + folderId,
      '{\"folder\": ' + JSON.stringify(this.folderNodeToJDTOPipe.transform(folderNode)) + '}',
      this.getValidatedHeader(validatedUser));
  }

  getFolderNode(folderId: number, validatedUser: ValidatedUser) {
    return this.http.get(this.baseHTTP_URL_File + 'nodes/folders?id=' + folderId,
      this.getValidatedHeader(validatedUser));
  }

  getFileNode(fileId: number, validatedUser: ValidatedUser) {
    return this.http.get(this.baseHTTP_URL_File + 'nodes/files/any?id=' + fileId,
      this.getValidatedHeader(validatedUser));
  }

  requestAllSections(validatedUser: ValidatedUser) {
    return this.http.get(this.baseHTTP_URL_File + 'nodes/sections',
      this.getValidatedHeader(validatedUser));
  }

  uploadFile(currentUser: ValidatedUser, fileNode: FileNode, file: File, folderNode: FolderNode | SectionNode, onProgress: (total, loaded) => void) {
    const url = this.baseHTTP_URL_File + 'data/raw/' + ((folderNode instanceof  SectionNode) ? 'section' : 'folder');
    const idName = (folderNode instanceof  SectionNode) ? 'sectionId' : 'folderId';
    return this.rawRequest('PUT',
      url + '?' + 'fileName=' + fileNode.name + '&dataType=' + fileNode.dataType + '&' + idName + '=' + folderNode.id,
      currentUser, file, onProgress);
  }

  startDownloadFileData(currentUser: ValidatedUser, fileNoteId: number): Observable<any> {
    return this.http.get(this.baseHTTP_URL_File + 'data/link?id=' + fileNoteId,
      this.getValidatedHeader(currentUser));
  }

  deleteFileNode(id: number, validatedUser: ValidatedUser) {
    return this.http.delete(this.baseHTTP_URL_File + 'nodes/files/any?id=' + id,
      this.getValidatedHeader(validatedUser));
  }

  updateFileNode(fileNodeId: number, fileNode: FileNode, validatedUser: ValidatedUser) {
    return this.http.post(this.baseHTTP_URL_File + 'nodes/files/any?id=' + fileNodeId,
      '{\"fileNode\": ' + JSON.stringify(this.fileNodeToJDTOPipe.transform(fileNode)) + '}',
      this.getValidatedHeader(validatedUser));
  }
}
