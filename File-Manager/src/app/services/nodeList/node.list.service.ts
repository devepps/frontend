import {Injectable} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {NodeF} from '../../models/model.node';
import {SectionNode} from '../../models/model.node.section';
import {FolderNode} from '../../models/model.node.folder';
import {SectionService} from '../section/section.service';
import {FolderService} from '../folder/folder.service';
import {ProgressInfo} from '../../models/model.progress.info';
import {FileNodeService} from '../file/file.node.service';
import {FileNode} from '../../models/model.node.file';
import {Sort} from '@angular/material';
import {SnackBarService} from '../../../lib/Base/src/app/services/utility/snackbar.service';
import {FileDataService} from '../fileData/file.data.service';
import {Path} from '../../models/model.path';
import {PathService} from '../path/path.service';
import {UtilityService} from '../../../lib/Base/src/app/services/utility/utility.service';

@Injectable({
  providedIn: 'root'
})
export  class NodeListService {

  private nodeList: Subject<NodeF[]> = new Subject<NodeF[]>();
  private lastNodeList: NodeF[] = [];

  private uploadingNodeMap: Map<string, NodeF[]> = new Map<string, NodeF[]>([]);

  private loadStatus: Subject<ProgressInfo> = new Subject<ProgressInfo>();
  private lastLoadStatus: ProgressInfo;

  private currentNode: NodeF;

  private sort: Sort;

  constructor(private sectionService: SectionService,
              private folderService: FolderService,
              private fileNodeService: FileNodeService,
              private snackBarService: SnackBarService,
              private fileDataService: FileDataService,
              private pathService: PathService,
              private utilityService: UtilityService) {
    const subscription = (folder: SectionNode | FolderNode) => {
      const loadStatus = new ProgressInfo(folder.fileNodeIds.length + folder.subFolderIds.length);
      const loadList: NodeF[] = [];
      this.nodeList.next(loadList);
      this.loadStatus.next(loadStatus);
      this.lastLoadStatus = loadStatus;
      const sort = this.sort;
      this.loadSubFolders(sort, folder.subFolderIds, loadList, loadStatus);
      this.loadFiles(sort, folder.fileNodeIds, loadList, loadStatus);
      this.manageUploadFileNodes(loadList);
      this.nodeList.next(loadList);
    };
    this.sectionService.getSelectedSection().subscribe(subscription);
    this.folderService.getSelectedFolder().subscribe(subscription);
    this.getList().subscribe((nodeList: NodeF[]) => {
      this.lastNodeList = nodeList;
    });
    this.pathService.getPath().subscribe((path: Path) => {
      this.currentNode = path.top();
    });
  }

  public getList(): Observable<NodeF[]> {
    return this.nodeList.asObservable();
  }

  public getProgress(): Observable<ProgressInfo> {
    return this.loadStatus.asObservable();
  }

  private loadSubFolders(sort: Sort, subFolderIds: number[], loadList: NodeF[], loadStatus: ProgressInfo) {
    for (const subFolderId of subFolderIds) {
      if (loadStatus !== this.lastLoadStatus) { break; }
      this.utilityService.delay(50);
      this.folderService.requestFolderNode(subFolderId, (folderNode: FolderNode) => {
        if (loadStatus === this.lastLoadStatus) {
          this.addSorted(sort, loadList, new NodeF(folderNode));
          this.nodeList.next(loadList);
          loadStatus.add();
        }
      }, (error) => {
        loadStatus.add();
      });
    }
  }

  private loadFiles(sort: Sort, fileNodeIds: number[], loadList: NodeF[], loadStatus: ProgressInfo) {
    for (const fileId of fileNodeIds) {
      if (loadStatus !== this.lastLoadStatus) { break; }
      this.utilityService.delay(50);
      this.fileNodeService.requestFileNode(fileId, (fileNode: FileNode) => {
        if (loadStatus === this.lastLoadStatus) {
          this.addSorted(sort, loadList, new NodeF(fileNode));
          this.nodeList.next(loadList);
          loadStatus.add();
        }
      }, (error) => {
        loadStatus.add();
      });
    }
  }

  private addSorted(sort: Sort, nodeList: NodeF[], node: NodeF) {
    if (this.sort) {
      let index = 0;
      let lastComparator = 0;
      while (index < nodeList.length) {
        const comparator = this.compare(sort, node, nodeList[index]);
        if ((comparator !== lastComparator && index !== 0) || (comparator === -1 && index === 0)) {
          break;
        }
        lastComparator = comparator;
        index = index + 1;
      }
      nodeList.splice(index, 0, node);
    } else {
      nodeList.push(node);
    }
  }

  private compare(sort: Sort, a: NodeF, b: NodeF): number {
    const isAsc = sort.direction === 'asc';
    switch (sort.active) {
      case 'Name':
        return (a.getName() < b.getName() ? -1 : 1) * (isAsc ? 1 : -1);
      case 'Datatype':
        return (a.getDataType() < b.getDataType() ? -1 : 1) * (isAsc ? 1 : -1);
      case 'Size':
        return (a.getSize() < b.getSize() ? -1 : 1) * (isAsc ? 1 : -1);
      case 'Created':
        if (typeof (a.getCreated()) === typeof (b.getCreated())) {
          return (a.getCreated() < b.getCreated() ? -1 : 1) * (isAsc ? 1 : -1);
        }
        if (a instanceof Date) {
          const isGeater = a.getCreated().getFullYear() > b.getCreated()[2] ? true :
            (a.getCreated().getMonth() > b.getCreated()[1] ? true :
              (a.getCreated().getDate() > b.getCreated()[0] ? true :
                (a.getCreated().getHours() > b.getCreated()[3] ? true :
                  a.getCreated().getMinutes() > b.getCreated()[4])));
          return (isGeater ? -1 : 1) * (isAsc ? 1 : -1);
        } else {
          const isGeater = b.getCreated().getFullYear() > a.getCreated()[2] ? true :
            (b.getCreated().getMonth() > a.getCreated()[1] ? true :
              (b.getCreated().getDate() > a.getCreated()[0] ? true :
                (b.getCreated().getHours() > a.getCreated()[3] ? true :
                  b.getCreated().getMinutes() > a.getCreated()[4])));
          return (isGeater ? 1 : -1) * (isAsc ? 1 : -1);
        }
      case 'NodeType':
        return ((a.isFolder() && b.isFile()) ? -1 : (b.isFolder() && a.isFile()) ? 1 : 0) * (isAsc ? 1 : -1);
      default:
        return 0;
    }
  }

  sortList(sort: Sort) {
    this.sort = sort;
    this.nodeList.next(this.lastNodeList.sort((a, b) => this.compare(sort, a, b)));
  }

  addUploading(fileNode: FileNode, file: File, currentNode?: NodeF) {
    const node = new NodeF(fileNode);
    node.uploadStatus = 0.0;
    currentNode = currentNode ? currentNode : this.currentNode;
    if (currentNode === null || currentNode === undefined) {
      this.snackBarService.openSnackBar('Failed to upload file: \"' + fileNode.name + '\" \n Context is missing');
    } else {
      const key = this.getKey(currentNode);
      let nodes = this.uploadingNodeMap.get(key);
      if (nodes === null || nodes === undefined) {
        nodes = [];
        this.uploadingNodeMap.set(key, nodes);
      }
      nodes.push(node);
      if (this.currentNode.getId() === currentNode.getId()) {
        this.addSorted(this.sort, this.lastNodeList, node);
        this.nodeList.next(this.lastNodeList);
      }
      this.fileDataService.uploadFileData(fileNode, file, currentNode.isFolder() ? currentNode.getFolderNode() : currentNode.getSectionNode(), (value: FileNode) => {
         this.uploadingNodeMap.set(key, this.uploadingNodeMap.get(key).filter((obj: NodeF) => obj !== node));
         if (currentNode.getId() === this.currentNode.getId()) {
           const newList = this.lastNodeList.filter((obj: NodeF) => obj !== node);
           this.addSorted(this.sort, newList, new NodeF(value));
           this.nodeList.next(newList);
         }
         if (value !== null && value !== undefined) {
           currentNode.getFileNodeIds().push(value.id);
         }
      }, (total, loaded) => {
        node.uploadStatus = loaded / total;
      });
    }
  }

  private getKey(currentNode: NodeF): string {
    return (currentNode.isSection() ? 's' : currentNode.isFolder() ? 'f' : currentNode.isFile() ? 'f' : 'u') + currentNode.getId();
  }

  private manageUploadFileNodes(loadList: NodeF[]) {
    if (this.currentNode !== null && this.currentNode !== undefined) {
      let uploadingList = this.uploadingNodeMap.get(this.getKey(this.currentNode));
      if (uploadingList !== null && uploadingList !== undefined) {
        uploadingList = uploadingList.filter((object: NodeF) => true);
        if (uploadingList !== null && uploadingList !== undefined && uploadingList.length > 0) {
          for (const node of uploadingList) {
            this.addSorted(this.sort, loadList, node);
          }
        }
      }
    }
  }

  addFolderNode(headNode: NodeF, folderNode: FolderNode, onSuccess?: (answer: FolderNode) => void) {
    this.folderService.addFolderNode(headNode, folderNode, (answerFolderNode: FolderNode) => {
      if (this.currentNode !== null && this.currentNode !== undefined) {
        if (onSuccess) {
          onSuccess(answerFolderNode);
        }
        if (headNode.isFolder()) {
          headNode.getFolderNode().subFolderIds.push(answerFolderNode.id);
        } else if (headNode.isSection()) {
          headNode.getSectionNode().subFolderIds.push(answerFolderNode.id);
        }
        if (this.currentNode.getId() === headNode.getId()) {
          this.addSorted(this.sort, this.lastNodeList, new NodeF(answerFolderNode));
          this.nodeList.next(this.lastNodeList);
        }
      }
    });
  }

  deleteFolderNode(folderNode: FolderNode) {
    this.folderService.deleteFolderNode(folderNode, () => {
      const nextFolderNodeList = this.lastNodeList.filter((node: NodeF) => !node.isFolder() || node.getFolderNode().id !== folderNode.id);
      if (nextFolderNodeList.length !== this.lastNodeList.length) {
        this.nodeList.next(nextFolderNodeList);
      }
    });
  }

  updateFolderNode(folderNodeId: number, folderNode: FolderNode) {
    this.folderService.updateFolderNode(folderNodeId, folderNode, (answerFolderNode: FolderNode) => {
      const nextFolderNodeList = this.lastNodeList.filter((node: NodeF) => !node.isFolder() || node.getFolderNode().id !== folderNode.id);
      if (nextFolderNodeList.length !== this.lastNodeList.length) {
        this.addSorted(this.sort, this.lastNodeList, new NodeF(answerFolderNode));
        this.nodeList.next(this.lastNodeList);
      }
    });
  }

  deleteFile(fileNode: FileNode, onSuccess?: () => void) {
    this.fileNodeService.deleteFileNode(fileNode, () => {
      const nextNodeList = this.lastNodeList.filter((node: NodeF) => !node.isFile() || node.getId() !== fileNode.id);
      if (nextNodeList.length !== this.lastNodeList.length) {
        this.nodeList.next(nextNodeList);
      }
      if (onSuccess) {
        onSuccess();
      }
    });
  }

  editFileNode(fileNodeId: number, fileNode: FileNode, onSuccess: (fileNode: FileNode) => void) {
    this.fileNodeService.updateFileNode(fileNodeId, fileNode, (answerFile: FileNode) => {
      const nextNodeList = this.lastNodeList.filter((node: NodeF) => !node.isFile() || node.getId() !== fileNode.id).concat();
      this.addSorted(this.sort, nextNodeList, new NodeF(answerFile));
      this.nodeList.next(nextNodeList);
      if (onSuccess) {
        onSuccess(answerFile);
      }
    });
  }
}
