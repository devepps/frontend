import { Injectable } from '@angular/core';
import {DTOService} from '../../lib/Base/src/app/services/network.dto.service';
import {SectionNodeJDTO} from '../models/jdto/model.jdto.node.section';
import {FolderNodeJDTO} from '../models/jdto/model.jdto.node.folder';
import {FileNodeJDTO} from '../models/jdto/model.jdto.node.file';

@Injectable({
  providedIn: 'root'
})
export class FileJDTOService extends DTOService {

  isSectionJDTOMessage(dto: any): dto is SectionNodeJDTO[] {
    const value = (<SectionNodeJDTO[]> dto);
    return value !== undefined && value[0] !== undefined;
  }

  isSectionJDTOListMessage(dto: any): dto is SectionNodeJDTO[] {
    const value = (<SectionNodeJDTO[]> dto);
    return value !== undefined;
  }

  isFolderJDTOMessage(dto: any): dto is FolderNodeJDTO[] {
    const value = (<FolderNodeJDTO[]> dto);
    return value !== undefined && value[0] !== undefined;
  }

  isFileJDTOMessage(dto: any): dto is FileNodeJDTO[] {
    const value = (<FileNodeJDTO[]> dto);
    return value !== undefined && value[0] !== undefined;
  }
}
