import {Injectable} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {SectionNode} from '../../models/model.node.section';
import {FileNetworkService} from '../network.file.service';
import {FileJDTOService} from '../network.file.jdto.service';
import {UserService} from '../../../lib/Base/src/app/services/user/user.service';
import {JdtoToSectionNodePipe} from '../../pipes/pipes.jdto.to.model';
import {SnackBarService} from '../../../lib/Base/src/app/services/utility/snackbar.service';
import {ValidatedUser} from '../../../lib/Base/src/app/models/model.validatedUser';

@Injectable({
  providedIn: 'root'
})
export  class NetworkSectionService {

  private sectionNodes: Subject<SectionNode[]> = new Subject<SectionNode[]>();
  private lastSectionNodes: SectionNode[] = [];

  private jdtoToSectionNodePipe = new JdtoToSectionNodePipe();

  constructor(private fileNetworkService: FileNetworkService,
              private fileJDTOService: FileJDTOService,
              private userService: UserService,
              private snackBarService: SnackBarService) {
    this.getSectionNodes().subscribe((sectionNodes: SectionNode[]) => {
      this.lastSectionNodes = sectionNodes;
    });
  }

  public getSectionNodes(): Observable<SectionNode[]> {
    return this.sectionNodes.asObservable();
  }

  addSectionNode(sectionNode: SectionNode) {
    this.fileNetworkService.addSectionNode(sectionNode, this.userService.getLastUser()).subscribe((value) => {
      if (this.fileJDTOService.isSectionJDTOMessage(value)) {
        const sectionNode = this.jdtoToSectionNodePipe.transform(value[0]);
        this.sectionNodes.next(this.lastSectionNodes.concat([sectionNode]));
      } else if (this.fileJDTOService.isExceptionResponseMessage(value)) {
        this.snackBarService.openSnackBar('Failed to add section "' + sectionNode.name + '"', value[0]);
      } else {
        this.snackBarService.openSnackBar('An unknown error occurred while adding section "' + sectionNode.name + '"', value[0]);
      }
    });
  }

  deleteSectionNode(sectionNode: SectionNode) {
    this.fileNetworkService.deleteSectionNode(sectionNode, this.userService.getLastUser()).subscribe((value) => {
      if (this.fileJDTOService.isEmptyMessage(value)) {
        this.snackBarService.openSnackBar('section "' + sectionNode.name + '" successfully deleted', value[0]);
        this.sectionNodes.next(this.lastSectionNodes.filter((section: SectionNode) => sectionNode.id !== section.id));
      } else if (this.fileJDTOService.isExceptionResponseMessage(value)) {
        this.snackBarService.openSnackBar('Failed to delete section "' + sectionNode.name + '"', value[0]);
      } else {
        this.snackBarService.openSnackBar('An unknown error occurred while deleting section "' + sectionNode.name + '"', value[0]);
      }
    });
  }

  updateSectionNode(sectionId: number, sectionNode: SectionNode) {
    this.fileNetworkService.updateSectionNode(sectionId, sectionNode, this.userService.getLastUser()).subscribe((value) => {
      if (this.fileJDTOService.isSectionJDTOMessage(value)) {
        const sectionNode = this.jdtoToSectionNodePipe.transform(value[0]);
        const sectionNodeList = this.lastSectionNodes.filter((sectionNode: SectionNode) => sectionNode.id !== sectionId);
        this.sectionNodes.next(sectionNodeList.concat([sectionNode]));
      } else if (this.fileJDTOService.isExceptionResponseMessage(value)) {
        this.snackBarService.openSnackBar('Failed to update section "' + sectionNode.name + '"', value[0]);
      } else {
        this.snackBarService.openSnackBar('An unknown error occurred while updating section "' + sectionNode.name + '"', value[0]);
      }
    });
  }

  requestAllSections(user: ValidatedUser) {
    this.fileNetworkService.requestAllSections(user).subscribe((value) => {
      if (this.fileJDTOService.isSectionJDTOListMessage(value)) {
        const sectionNodes = [];
        for (const node of value) {
          sectionNodes.push(this.jdtoToSectionNodePipe.transform(node));
        }
        this.sectionNodes.next(sectionNodes);
      } else if (this.fileJDTOService.isExceptionResponseMessage(value)) {
        this.snackBarService.openSnackBar('Failed to request sections', value[0]);
      } else {
        this.snackBarService.openSnackBar('An unknown error occurred while requesting section', value[0]);
      }
    });
  }
}
