import {Injectable} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {SectionNode} from '../../models/model.node.section';
import {NetworkSectionService} from './network.section.service';
import {ValidatedUser} from '../../../lib/Base/src/app/models/model.validatedUser';

@Injectable({
  providedIn: 'root'
})
export  class SectionService {

  private selectedSection: Subject<SectionNode> = new Subject<SectionNode>();
  private lastSelectedSectionNode: SectionNode;

  constructor(private networkSectionService: NetworkSectionService) {
    this.networkSectionService.getSectionNodes().subscribe((sectionNodes: SectionNode[]) => {
      if (this.lastSelectedSectionNode !== null && this.lastSelectedSectionNode !== undefined &&
        sectionNodes.filter((sectionNode: SectionNode) => sectionNode.id === this.lastSelectedSectionNode.id).length === 0) {
        this.selectSection(null);
      }
    });
    this.getSelectedSection().subscribe((selectedSection: SectionNode) => {
      this.lastSelectedSectionNode = selectedSection;
    });
  }

  public getSectionNodes(): Observable<SectionNode[]> {
    return this.networkSectionService.getSectionNodes();
  }

  public getSelectedSection(): Observable<SectionNode> {
    return this.selectedSection.asObservable();
  }

  public selectSection(section: SectionNode): void {
    return this.selectedSection.next(section);
  }

  addSectionNode(sectionNode: SectionNode) {
    this.networkSectionService.addSectionNode(sectionNode);
  }

  deleteSectionNode(sectionNode: SectionNode) {
    this.networkSectionService.deleteSectionNode(sectionNode);
  }

  updateSectionNode(sectionNodeId: number, sectionNode: SectionNode) {
    this.networkSectionService.updateSectionNode(sectionNodeId, sectionNode);
  }

  requestAllSections(user: ValidatedUser) {
    this.networkSectionService.requestAllSections(user);
  }

  getCurrentNode() {
    return this.lastSelectedSectionNode;
  }
}
