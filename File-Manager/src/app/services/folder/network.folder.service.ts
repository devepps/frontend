import {Injectable} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {FileNetworkService} from '../network.file.service';
import {FileJDTOService} from '../network.file.jdto.service';
import {UserService} from '../../../lib/Base/src/app/services/user/user.service';
import {SnackBarService} from '../../../lib/Base/src/app/services/utility/snackbar.service';
import {FolderNode} from '../../models/model.node.folder';
import {JdtoToFolderNodePipe} from '../../pipes/pipes.jdto.to.model';
import {NodeF} from '../../models/model.node';

@Injectable({
  providedIn: 'root'
})
export  class NetworkFolderService {

  private jdtoToFolderNodePipe = new JdtoToFolderNodePipe();

  constructor(private fileNetworkService: FileNetworkService,
              private fileJDTOService: FileJDTOService,
              private userService: UserService,
              private snackBarService: SnackBarService) {
  }

  addFolderNode(headNode: NodeF, folderNode: FolderNode, onSuccess?: (FolderNode) => void, onError?: (any) => void) {
    this.fileNetworkService.addFolderNode(headNode, folderNode, this.userService.getLastUser()).subscribe((value) => {
      if (this.fileJDTOService.isFolderJDTOMessage(value)) {
        if (onSuccess) { onSuccess(this.jdtoToFolderNodePipe.transform(value[0])); }
      } else if (this.fileJDTOService.isExceptionResponseMessage(value)) {
        if (onError) { onError(value); }
        this.snackBarService.openSnackBar('Failed to add folder "' + folderNode.name + '"', value[0]);
      } else {
        if (onError) { onError(null); }
        this.snackBarService.openSnackBar('An unknown error occurred while adding folder "' + folderNode.name + '"', value[0]);
      }
    });
  }

  deleteFolderNode(folderNode: FolderNode, onSuccess?: () => void, onError?: (any) => void) {
    this.fileNetworkService.deleteFolderNode(folderNode, this.userService.getLastUser()).subscribe((value) => {
      if (this.fileJDTOService.isEmptyMessage(value)) {
        if (onSuccess) { onSuccess(); }
      } else if (this.fileJDTOService.isExceptionResponseMessage(value)) {
        if (onError) { onError(value); }
        this.snackBarService.openSnackBar('Failed to delete folder "' + folderNode.name + '"', value[0]);
      } else {
        if (onError) { onError(value); }
        this.snackBarService.openSnackBar('An unknown error occurred while deleting folder "' + folderNode.name + '"', value[0]);
      }
    });
  }

  updateFolderNode(folderId: number, folderNode: FolderNode, onSuccess?: (FolderNode) => void, onError?: (any) => void) {
    this.fileNetworkService.updateFolderNode(folderId, folderNode, this.userService.getLastUser()).subscribe((value) => {
      if (this.fileJDTOService.isFolderJDTOMessage(value)) {
        const folderNode = this.jdtoToFolderNodePipe.transform(value[0]);
        if (onSuccess) { onSuccess(folderNode); }
      } else if (this.fileJDTOService.isExceptionResponseMessage(value)) {
        if (onError) { onError(value); }
        this.snackBarService.openSnackBar('Failed to update folder "' + folderNode.name + '"', value[0]);
      } else {
        if (onError) { onError(null); }
        this.snackBarService.openSnackBar('An unknown error occurred while updating folder "' + folderNode.name + '"', value[0]);
      }
    });
  }

  requestFolderNode(folderId: number, onSuccess?: (FolderNode) => void, onError?: (any) => void) {
    this.fileNetworkService.getFolderNode(folderId, this.userService.getLastUser()).subscribe((value) => {
      if (this.fileJDTOService.isFolderJDTOMessage(value)) {
        const folderNode = this.jdtoToFolderNodePipe.transform(value[0]);
        if (onSuccess) { onSuccess(folderNode); }
      } else if (this.fileJDTOService.isExceptionResponseMessage(value)) {
        if (onError) { onError(value); }
        this.snackBarService.openSnackBar('Failed to request folder', value[0]);
      } else {
        if (onError) { onError(null); }
        this.snackBarService.openSnackBar('An unknown error occurred while requesting folder', value[0]);
      }
    });
  }
}
