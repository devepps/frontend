import {Injectable} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {NetworkFolderService} from '../folder/network.folder.service';
import {FolderNode} from '../../models/model.node.folder';
import {NodeF} from '../../models/model.node';
import {NodeListService} from '../nodeList/node.list.service';

@Injectable({
  providedIn: 'root'
})
export  class FolderService {

  private selectedFolder: Subject<FolderNode> = new Subject<FolderNode>();
  private lastSelectedFolder: FolderNode;

  constructor(private networkFolderService: NetworkFolderService) {
    this.getSelectedFolder().subscribe((selectedFolder: FolderNode) => {
      this.lastSelectedFolder = selectedFolder;
    });
  }

  public getSelectedFolder(): Observable<FolderNode> {
    return this.selectedFolder.asObservable();
  }

  public selectFolder(folder: FolderNode): void {
    return this.selectedFolder.next(folder);
  }

  addFolderNode(headNode: NodeF, folderNode: FolderNode, onSuccess?: (folderNode: FolderNode) => void, onError?: (any) => void) {
    this.networkFolderService.addFolderNode(headNode, folderNode, onSuccess, onError);
  }

  deleteFolderNode(folderNode: FolderNode, onSuccess?: () => void, onError?: (any) => void) {
    this.networkFolderService.deleteFolderNode(folderNode, onSuccess, onError);
  }

  updateFolderNode(folderNodeId: number, folderNode: FolderNode, onSuccess?: (FolderNode) => void, onError?: (any) => void) {
    this.networkFolderService.updateFolderNode(folderNodeId, folderNode, onSuccess, onError);
  }

  requestFolderNode(folderId: number, onSuccess?: (FolderNode) => void, onError?: (any) => void) {
    this.networkFolderService.requestFolderNode(folderId, onSuccess, onError);
  }

  getCurrentNode(): FolderNode {
    return this.lastSelectedFolder;
  }
}
