import {Pipe, PipeTransform} from '@angular/core';
import {FileNodeJDTO} from '../models/jdto/model.jdto.node.file';
import {FileNode} from '../models/model.node.file';
import {FolderNode} from '../models/model.node.folder';
import {FolderNodeJDTO} from '../models/jdto/model.jdto.node.folder';
import {SectionNode} from '../models/model.node.section';
import {SectionNodeJDTO} from '../models/jdto/model.jdto.node.section';

@Pipe({name: 'fileNodeToJDTO'})
export class FileNodeToJDTOPipe implements PipeTransform {
  transform(model: FileNode ): FileNodeJDTO {
    if (model) {
      return {
        id: model.id,
        name: model.name,
        dataType: model.dataType,
        size: model.size,
        created: ( model.created !== null && model.created !== undefined ) ?
          [model.created.getFullYear(), model.created.getMonth(), model.created.getDate(), model.created.getHours(), model.created.getMinutes(), model.created.getSeconds()]
          : null,
      };
    } else {
      return {
        id: null,
        name: null,
        dataType: null,
        size: null,
        created: null,
      };
    }
  }
}

@Pipe({name: 'folderNodeToJDTO'})
export class FolderNodeToJDTOPipe implements PipeTransform {
  transform(model: FolderNode ): FolderNodeJDTO {
    if (model) {
      return {
        id: model.id,
        name: model.name,
        created: ( model.created !== null && model.created !== undefined ) ?
          [model.created.getFullYear(), model.created.getMonth(), model.created.getDate(), model.created.getHours(), model.created.getMinutes(), model.created.getSeconds()]
          : null,
        subFolderIds: model.subFolderIds,
        fileNodeIds: model.fileNodeIds,
      };
    } else {
      return {
        id: null,
        name: null,
        created: null,
        subFolderIds: null,
        fileNodeIds: null,
      };
    }
  }
}

@Pipe({name: 'sectionNodeToJDTO'})
export class SectionNodeToJDTOPipe implements PipeTransform {
  transform(model: SectionNode ): SectionNodeJDTO {
    if (model) {
      return {
        id: model.id,
        name: model.name,
        created: ( model.created !== null && model.created !== undefined ) ?
          [model.created.getFullYear(), model.created.getMonth(), model.created.getDate(), model.created.getHours(), model.created.getMinutes(), model.created.getSeconds()]
          : null,
        subFolderIds: model.subFolderIds,
        fileNodeIds: model.fileNodeIds,
      };
    } else {
      return {
        id: null,
        name: null,
        created: null,
        subFolderIds: null,
        fileNodeIds: null,
      };
    }
  }
}
