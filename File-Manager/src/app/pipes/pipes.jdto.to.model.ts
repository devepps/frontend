import {Pipe, PipeTransform} from '@angular/core';
import {FileNode} from '../models/model.node.file';
import {FileNodeJDTO} from '../models/jdto/model.jdto.node.file';
import {FolderNodeJDTO} from '../models/jdto/model.jdto.node.folder';
import {FolderNode} from '../models/model.node.folder';
import {SectionNodeJDTO} from '../models/jdto/model.jdto.node.section';
import {SectionNode} from '../models/model.node.section';

@Pipe({name: 'jdtoToFileNode'})
export class JdtoToFileNodePipe implements PipeTransform {
  transform(model: FileNodeJDTO ): FileNode {
    if (model) {
      return new FileNode(model.id, model.name, model.dataType, model.size,
        (model.created === null) ? null : new Date(model.created[0], model.created[1] - 1, model.created[2], model.created[3], model.created[4], model.created[5]));
    } else {
      return new FileNode(0, '', '', null, null);
    }
  }
}

@Pipe({name: 'jdtoToFolderNode'})
export class JdtoToFolderNodePipe implements PipeTransform {
  transform(model: FolderNodeJDTO ): FolderNode {
    if (model) {
      return new FolderNode(model.id, model.name,
        (model.created === null) ? null : new Date(model.created[0], model.created[1] - 1, model.created[2], model.created[3], model.created[4], model.created[5]),
                model.fileNodeIds, model.subFolderIds);
    } else {
      return new FolderNode(0, '', null, [], []);
    }
  }
}

@Pipe({name: 'jdtoToSectionNode'})
export class JdtoToSectionNodePipe implements PipeTransform {
  transform(model: SectionNodeJDTO ): SectionNode {
    if (model) {
      return new SectionNode(model.id, model.name,
        (model.created === null) ? null : new Date(model.created[0], model.created[1] - 1, model.created[2], model.created[3], model.created[4], model.created[5]),
        model.fileNodeIds, model.subFolderIds);
    } else {
      return new SectionNode(0, '', null, [], []);
    }
  }
}
