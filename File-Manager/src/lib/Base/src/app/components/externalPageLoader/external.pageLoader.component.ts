import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {DomSanitizer, SafeResourceUrl} from '@angular/platform-browser';


@Component({
  selector: 'external-window',
  templateUrl: './external.pageLoader.component.html',
  styleUrls: ['./external.pageLoader.component.css']
})
export class ExternalPageLoaderComponent implements OnInit {

  private url: SafeResourceUrl;
  private lastURL = '';

  ngOnInit(): void {
    this.url = this.sanitizer.bypassSecurityTrustResourceUrl('https://reschiram.ddns.net' + this.router.url.toLowerCase() + '-frontend');
    this.lastURL = 'https://reschiram.ddns.net' + this.router.url.toLowerCase() + '-frontend';
    this.router.events.subscribe((event) => {
      const newURL = 'https://reschiram.ddns.net' + this.router.url.toLowerCase() + '-frontend';
      if (newURL !== this.lastURL) {
        this.url = this.sanitizer.bypassSecurityTrustResourceUrl(newURL);
        this.lastURL = newURL;
      }
    });
  }

  constructor(
    private router: Router,
    private sanitizer: DomSanitizer)
  {}
}
