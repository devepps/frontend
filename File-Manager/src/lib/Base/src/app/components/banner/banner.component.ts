import { Component } from '@angular/core';
import { UserService } from '../../services/user/user.service';
import {ValidatedUser} from '../../models/model.validatedUser';
import {LoginDialogService} from '../../services/dialog/dialog.login.service';

@Component({
  selector: 'banner',
  templateUrl: './banner.component.html',
  styleUrls: ['./banner.component.css']
})
export class BannerComponent {

  private currentUser: ValidatedUser = null;

  constructor(private userService: UserService,
              private loginDialogService: LoginDialogService) {
    this.userService.getCurrentUser().subscribe((currentUser: ValidatedUser) => {
      this.currentUser = currentUser;
    });
  }

  private isLoggedOut(): boolean {
    return this.currentUser == null;
  }

  private isLoggedIn() {
    return this.currentUser != null;
  }

  private logout(): void {
    this.userService.logout();
  }

  private login(): void {
    this.loginDialogService.openLoginDialog();
  }

  redirectTo(href: string) {
    window.location.href = href;
  }
}
