import { Route } from '@angular/compiler/src/core';
import { UrlSegment, CanLoad } from '@angular/router';
import { Observable } from 'rxjs';
import { UserService } from '../services/user/user.service';

export class RouterLoadLogin implements CanLoad {

  constructor(private userService: UserService) { }

  canLoad(route: Route, segments: UrlSegment[]): Observable<boolean> | Promise<boolean> | boolean {
    return this.userService.isLoggedout();
  }

}

export class RouterLoadRegister implements CanLoad {

  constructor(private userService: UserService) { }

  canLoad(route: Route, segments: UrlSegment[]): Observable<boolean> | Promise<boolean> | boolean {
    return this.userService.isLoggedout();
  }

}

