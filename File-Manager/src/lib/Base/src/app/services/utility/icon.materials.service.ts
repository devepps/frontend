import { Injectable } from '@angular/core';
import {MatIconRegistry} from '@angular/material';
import {DomSanitizer} from '@angular/platform-browser';

@Injectable({
  providedIn: 'root'
})
export class IconMaterialsService {

  private baseURL = 'assets/';

  constructor(private iconRegistry: MatIconRegistry,
              private sanitizer: DomSanitizer) {}

  public register(iconName: string, iconURL): void {
    this.iconRegistry.addSvgIcon(iconName, this.sanitizer.bypassSecurityTrustResourceUrl(iconURL));
  }

  public registerAll(): void {
    this.register('delete_forever', this.baseURL + 'icon/delete_forever-icon.svg');
    this.register('edit', this.baseURL + 'icon/edit-icon.svg');
    this.register('visible', this.baseURL + 'icon/visible-icon.svg');
    this.register('not_visible', this.baseURL + 'icon/not-visible-icon.svg');
    this.register('arrow_down', this.baseURL + 'icon/arrow-down-icon.svg');
    this.register('arrow_up', this.baseURL + 'icon/arrow-up-icon.svg');
    this.register('clipboard_copy', this.baseURL + 'icon/clipboard-copy-icon.svg');
    this.register('upload', this.baseURL + 'icon/upload-icon.svg');
    this.register('download', this.baseURL + 'icon/download-icon.svg');
  }
}
