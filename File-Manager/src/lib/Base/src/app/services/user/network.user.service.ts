import { Injectable } from '@angular/core';
import { NetworkService } from '../network.service';
import { ValidatedUser } from '../../models/model.validatedUser';
import { DTOService } from '../network.dto.service';
import { CookieService } from 'ngx-cookie-service';
import { Validation } from '../../models/model.validation';
import { User } from '../../models/model.user';
import { Router } from '@angular/router';
import {Observable, Subject} from 'rxjs';
import {SnackBarService} from '../utility/snackbar.service';
import {Login} from '../../models/model.login';

const userIdCP = 'security-user-id';
const validationKeyCP = 'security-user-validationKey';

@Injectable({
  providedIn: 'root'
})
export class NetworkUserService {

  private currentUser: Subject<ValidatedUser> = new Subject<ValidatedUser>();

  private last_validation_key = '';
  private last_user_id = '';

  constructor(
    private networkService: NetworkService,
    private dtoService: DTOService,
    private cookieService: CookieService,
    private router: Router,
    private snackBarService: SnackBarService) {

    this.getCurrentUser().subscribe((validatedUser: ValidatedUser) => {
      if (validatedUser) {
        this.last_user_id = validatedUser.userId + '';
        this.last_validation_key = validatedUser.validationKey + '';
        this.cookieService.set(userIdCP, validatedUser.userId + '');
        this.cookieService.set(validationKeyCP, validatedUser.validationKey + '');
      }
    });

    const loop_function = function (that) {
      const current_validation_key = that.cookieService.get(validationKeyCP);
      const current_user_id = that.cookieService.get(userIdCP);
      if (current_user_id !== that.last_user_id || current_validation_key !== that.last_validation_key) {
        console.debug('Validation');
        that.last_user_id = current_user_id;
        that.last_validation_key = current_validation_key;
        that.validate();
      }
      setTimeout(function () {
        loop_function(that);
      }, 100);
    };
    loop_function(this);

    this.validate();
  }

  public validate(): void {
    if (this.cookieService.check(userIdCP) && this.cookieService.check(validationKeyCP)) {
      const validationRequest: Validation = new Validation(this.cookieService.get(userIdCP),
        this.cookieService.get(validationKeyCP));
      this.networkService.validateUser(validationRequest).subscribe(validation => {
        if (this.dtoService.isValidatedUserResponseMessage(validation)) {
          this.currentUser.next(validation[0]);
        } else if (this.dtoService.isExceptionResponseMessage(validation)) {
          this.snackBarService.openSnackBar('User-Validation failed.', validation[0]);
        }
      });
    }
  }

  public login(loginData: Login, onSuccess?: () => void): void {
    const validatedUser: ValidatedUser = new ValidatedUser(loginData.username, null, null);
    this.networkService.login(loginData).subscribe(validation => {
      this.snackBarService.openSnackBar('Login: ' + validation);
      if (this.dtoService.isValidationMessage(validation)) {
        validatedUser.setValidation(validation[0]);
        this.currentUser.next(validatedUser);
        if (onSuccess) {
          onSuccess();
        }
        this.snackBarService.openSnackBar('Succesfully Logged In');
      } else if (this.dtoService.isExceptionResponseMessage(validation)) {
        this.snackBarService.openSnackBar('Login Failed', validation[0]);
      } else {
        this.snackBarService.openSnackBar('Login Failed');
      }
    });
  }

  public register(userData: User, onSuccess?: () => void): void {
    const validatedUser: ValidatedUser = new ValidatedUser(userData.username, null, null);
    this.networkService.register(userData).subscribe(validation => {
      if (this.dtoService.isValidationMessage(validation)) {
        validatedUser.setValidation(validation[0]);
        this.currentUser.next(validatedUser);
        if (onSuccess) {
          onSuccess();
        }
        this.snackBarService.openSnackBar('Succesfully Registered As ' + userData.username);
      } else if (this.dtoService.isExceptionResponseMessage(validation)) {
        this.snackBarService.openSnackBar('Registration Failed', validation[0]);
      } else {
        this.snackBarService.openSnackBar('Registration Failed');
      }
    });
  }

  public getCurrentUser(): Observable<ValidatedUser> {
    return this.currentUser.asObservable();
  }

  public logout(): void {
    this.cookieService.delete(userIdCP);
    this.cookieService.delete(validationKeyCP);
    this.currentUser.next(null);
  }
}
