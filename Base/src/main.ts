import 'hammerjs';
import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppBaseModule } from './app/root/app.base.module';

enableProdMode();

platformBrowserDynamic().bootstrapModule(AppBaseModule)
  .catch(err => console.log(err));
