import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../../services/user/user.service';
import { Login } from '../../models/model.login';


@Component({
  selector: 'login-window',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {

  private loginData: Login = new Login('', '');

  constructor(
    private router: Router,
    private userService: UserService)
  { }

  private login(): void {
    this.userService.login(this.loginData, () => {
      this.router.navigateByUrl('');
    });
  }
}
