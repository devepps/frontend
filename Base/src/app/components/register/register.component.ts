import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import { UserService } from '../../services/user/user.service';
import { User } from '../../models/model.user';


@Component({
  selector: 'register-window',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  userData: User = new User('', '', '');

  ngOnInit(): void {}

  constructor(
    private router: Router,
    private userService: UserService)
  { }

  register(): void {
    this.userService.register(this.userData, () => {
      this.router.navigateByUrl('');
    });
  }
}
