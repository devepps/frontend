export class Validation {

  private _userId: string;
  private _validationKey: string;

  constructor(userId: string, validationKey: string) {
    this._userId = userId;
    this._validationKey = validationKey;
  }

  get userId(): string {
    return this._userId;
  }

  get validationKey(): string {
    return this._validationKey;
  }
}
