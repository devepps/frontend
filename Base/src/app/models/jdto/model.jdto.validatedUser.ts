export class ValidatedUserJDTO {

  public username: string;
  public userId: string;
  public validationKey: string;
}
