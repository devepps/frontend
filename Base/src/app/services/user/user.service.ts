import { Injectable } from '@angular/core';
import { ValidatedUser } from '../../models/model.validatedUser';
import { SnackBarService } from '../utility/snackbar.service';
import { User } from '../../models/model.user';
import {Observable} from 'rxjs';
import {NetworkUserService} from './network.user.service';
import {Login} from '../../models/model.login';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private isLoggedIn = false;
  private lastUser: ValidatedUser;

  constructor(
    private networkUserService: NetworkUserService,
    private snackBarService: SnackBarService) {
    this.networkUserService.getCurrentUser().subscribe((currentUser: ValidatedUser) => {
      if (currentUser) {
        this.isLoggedIn = true;
      } else {
        this.isLoggedIn = false;
      }
      this.lastUser = currentUser;
    });
  }

  public login(loginData: Login, onSuccess?: () => void): void {
    if (this.isLoggedin()) {
      this.snackBarService.openSnackBar('Bereits eingeloggt.');
      return;
    }
    this.networkUserService.login(loginData, onSuccess);
  }

  public register(userData: User, onSuccess?: () => void): void {
    if (this.isLoggedin()) {
      this.snackBarService.openSnackBar('Bereits eingeloggt.');
      return;
    }
    this.networkUserService.register(userData, onSuccess);
  }

  public isLoggedin(): boolean {
    return this.isLoggedIn;
  }

  public isLoggedout(): boolean {
    return !this.isLoggedIn;
  }

  public getCurrentUser(): Observable<ValidatedUser> {
    return this.networkUserService.getCurrentUser();
  }

  public getLastUser(): ValidatedUser {
    return this.lastUser;
  }

  public logout(): void {
    this.networkUserService.logout();
  }

  public validate(): void {
    this.networkUserService.validate();
  }
}
