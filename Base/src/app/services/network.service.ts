import { Injectable } from '@angular/core';
import { Login } from '../models/model.login';
import { HttpClient } from '@angular/common/http';
import {Observable, Subject} from 'rxjs';
import { Validation } from '../models/model.validation';
import { HttpHeaders } from '@angular/common/http';
import { User } from '../models/model.user';
import { ValidatedUser } from '../models/model.validatedUser';
import {
  LoginToJDTOPipe,
  UserToJDTOPipe,
  ValidationToJDTOPipe
} from '../pipes/pipes.model.to.jdto';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class NetworkService {

  protected httpOptions_jsonContent = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    })
  };

  protected baseHTTP_URL_User: String = environment.baseHTTP_URL_User;

  protected loginToJDTOPipe = new LoginToJDTOPipe();
  protected userToJDTOPipe = new UserToJDTOPipe();
  protected validationToJDTOPipe = new ValidationToJDTOPipe();

  constructor(
    protected http: HttpClient) { }

  login(loginData: Login): Observable<any> {
    return this.http.post(this.baseHTTP_URL_User + 'user/login',
      '{\"login\": ' + JSON.stringify(this.loginToJDTOPipe.transform(loginData)) + '}',
      this.httpOptions_jsonContent);
  }

  validateUser(validationData: Validation): Observable<any> {
    return this.http.post(this.baseHTTP_URL_User + 'user/validation',
      '{\"validation\": ' + JSON.stringify(this.validationToJDTOPipe.transform(validationData)) + '}',
      this.httpOptions_jsonContent);
  }

  register(userData: User): Observable<any> {
    return this.http.put(this.baseHTTP_URL_User + 'user/create',
      '{\"user\": ' + JSON.stringify(this.userToJDTOPipe.transform(userData)) + '}',
      this.httpOptions_jsonContent);
  }

  getValidatedHeader(currentUser: ValidatedUser, contentLength?: number): any {
    return (contentLength) ? {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorisation-User-Id': currentUser.userId,
        'Authorisation-User-ValidationKey': currentUser.validationKey,
        'Raw-Data-Length': contentLength + '',
        'encoding': 'iso-8859-1'
      })
    } : {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorisation-User-Id': currentUser.userId,
        'Authorisation-User-ValidationKey': currentUser.validationKey,
      })
    };
  }

  getValidatedDownloadHeader(currentUser: ValidatedUser): any {
    return {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorisation-User-Id': currentUser.userId,
        'Authorisation-User-ValidationKey': currentUser.validationKey,
      }),
      'responseType': 'application/octet-stream'
    };
  }

  rawRequest(method: string, url: string, currentUser: ValidatedUser, data, onProgress?: (total, loaded) => any): Observable<any> {
    let subject = new Subject<any>();
    const xhttp = new XMLHttpRequest();
    xhttp.open(method, url, true);
    xhttp.setRequestHeader('Authorisation-User-Id', currentUser.userId);
    xhttp.setRequestHeader('Authorisation-User-ValidationKey', currentUser.validationKey);
    xhttp.onreadystatechange = function() {
      try {
        if (subject != null) {
          subject.next(JSON.parse(xhttp.response));
          subject = null;
        }
      } catch (e) {}
    };
    if (onProgress) {
      xhttp.upload.onprogress = function (event) {
        onProgress(event.total, event.loaded);
      };
    }
    xhttp.send(data);
    return subject.asObservable();
  }

}
