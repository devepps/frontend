import { Injectable } from '@angular/core';
import {Validation} from '../models/model.validation';
import {ExceptionResponse} from '../exceptions/response.error';
import {ValidatedUser} from '../models/model.validatedUser';

@Injectable({
  providedIn: 'root'
})
export class DTOService {

  public isValidationMessage(validation: any): validation is Validation[] {
    const value = (<Validation[][]>validation);
    return value !== undefined && value[0] !== undefined;
  }

  public isExceptionResponseMessage(error: any): error is ExceptionResponse[] {
    const value =  (<ExceptionResponse[]>error);
    return value !== undefined && value[0] !== undefined;
  }

  public isValidatedUserResponseMessage(validation: any): validation is ValidatedUser[] {
    const value =  (<ValidatedUser[]>validation);
    return value !== undefined && value[0] !== undefined;
  }

  public isEmptyMessage(message: any): message is any[] {
    const value =  (<any[]> message);
    return value !== undefined && value.length === 0;
  }
}
