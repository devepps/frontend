import {Injectable} from '@angular/core';
import {MatDialog} from '@angular/material';
import {DeleteDialogInfo} from '../../models/model.dialog.info.delete';
import {DeleteDialogComponent} from '../../components/deletePopup/dialog.delete';

@Injectable({
  providedIn: 'root'
})
export class StandardDialogService {

  constructor(private dialog: MatDialog) {}

  public openDeleteDialog(deleteDialogInfo: DeleteDialogInfo): void {
    let ref = this.dialog.open(DeleteDialogComponent, { data: deleteDialogInfo});
    ref.afterClosed().subscribe(() => {
      if (ref) {
        ref = null;
      }
    });
    ref.backdropClick().subscribe(() => {
      if (ref) {
        ref.close();
      }
    });
  }

  public openRawDeleteDialog(name: string, deleteMethod: (onSuccess: () => void) => any): void {
    this.openDeleteDialog(new DeleteDialogInfo(name, deleteMethod));
  }
}
