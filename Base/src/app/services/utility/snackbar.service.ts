import { Injectable } from '@angular/core';
import { MatSnackBar, MatSnackBarRef } from '@angular/material/snack-bar';
import {ExceptionResponse} from '../../exceptions/response.error';

@Injectable({
  providedIn: 'root'
})
export class SnackBarService {

  private duration = 10000;

  constructor(private snackBar: MatSnackBar) { }

  public openSnackBar(message: string, error?: ExceptionResponse): MatSnackBarRef<any> {
    if (error !== undefined && error !== null) {
      const snackBar: MatSnackBarRef<any> = this.snackBar.open(message, 'Detail', {
        duration: this.duration,
      });
      snackBar.onAction().subscribe(() => {
        this.openSnackBar(error.message);
      });
      return snackBar;
    } else {
      const snackBar: MatSnackBarRef<any> = this.snackBar.open(message, 'OK', {
        duration: this.duration,
      });
      snackBar.onAction().subscribe(() => {
        snackBar.dismiss();
      });
      return snackBar;
    }
  }
}
