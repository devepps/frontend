import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpHandler, HttpRequest, HttpErrorResponse} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SnackBarService } from '../services/utility/snackbar.service';
import { DTOService } from '../services/network.dto.service';
import 'rxjs-compat/add/operator/catch';
import {of} from 'rxjs';
import {ExceptionResponse} from '../exceptions/response.error';

@Injectable()
export class HttpErrorInterceptor implements HttpInterceptor {

  constructor(private snackBarService: SnackBarService,
    private dtoService: DTOService) { }

  public intercept (request: HttpRequest<any>, next: HttpHandler): Observable<any> {
    return next.handle(request)
      .catch((response, caught) => {
        if (! (response instanceof HttpErrorResponse)) {
          this.snackBarService.openSnackBar('An unexpected error occurred.');
          return new Observable<any>();
        } else {
          if (!this.dtoService.isExceptionResponseMessage(response.error)) {
            this.snackBarService.openSnackBar('An error occurred: ' + response.message);
            return new Observable<any>();
          } else {
            if (response.error[0].message !== undefined && response.error[0].message !== null && response.error[0].message.trim().length > 0) {
              this.snackBarService.openSnackBar(response.error[0].message);
            }
            return of(response.error as ExceptionResponse[]);
          }
        }
      });
  }
}
